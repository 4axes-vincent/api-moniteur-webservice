#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QSettings>
#include <QString>
#include <QStringList>
#include <QCoreApplication>
#include <QStringBuilder>
#include <QMessageBox>

class Configuration
{
public:
    Configuration();

    QString getPathNas();

    QString getPathRelance();

    QString getPathRenvoi();

    QStringList getListeCle(QString fichierINI, QString groupe);

    QString getValue(QString fichierINI, QString groupe, QString key);

    bool siGroupeExiste(QString fichierINI, QString groupe);

    bool siCleExiste(QString fichierINI, QString groupe, QString cle);
};

#endif // CONFIGURATION_H
