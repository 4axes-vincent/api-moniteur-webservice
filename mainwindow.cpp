#include "mainwindow.h"
#include "ui_mainwindow.h"
//----------------------------------------------------------------------------------------------------------------------






//----------------------------------------------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle ("Montieur 4axes - WebService");

    ui->statusBar->showMessage ( ADR_HTTP_RETOUR_XML % " & " % ADR_HTTP_RECONCILIER);

    //Liste des arguments
    QStringList listeArguments = QCoreApplication::arguments ();

    //Affichage du chemin d'acc�s du NAS
    ui->lineEdit_pathNAS->setText ( config.getPathNas () );
    ui->lineEdit_pathNAS->setAlignment ( Qt::AlignCenter );

    ui->lineEdit_action->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_nbRelance->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_relanceEnCours->setAlignment ( Qt::AlignCenter );

    ui->lineEdit_adrSite->setAlignment( Qt::AlignCenter );

    //Connexion avec la bdd
    connect (&OBJ_bdd, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

    OBJ_bdd.creerTable();

    //Archivage fichier log de la veille
    QFileInfo infoFichierLog( NOM_FICHIER_LOG );
    if( infoFichierLog.lastModified ().toString ("yyyyMMdd") < QDateTime::currentDateTime ().toString ("yyyyMMdd") )
        QFile::rename ( infoFichierLog.absoluteFilePath (), infoFichierLog.absoluteFilePath ().append ( infoFichierLog.lastRead ().toString ("yyyyMMdd") ) % ".log" );

    afficherMessage("--------------------------------", false);
    afficherMessage("Lancement FRONTAL", false);
    afficherMessage("*******", false);

    chrono = new QTime();

    nbErreurEnvoi = 0;
    nbRelanceEnCours = 0;

    siRelance = false;
    renvoiRpec = false;
    receptionMail = false;

    //Dans le cas de la relance
    if( listeArguments.length () > 1 ){

        if( listeArguments.at (1) == "relance" )
            lancerRelance ();

        else if( listeArguments.at (1) == "auto" )
            lancerAPI ();

        else if ( listeArguments.at (1) == "archiver" )
            lancerArchivage();

        else if( listeArguments.at (1) == "recupErreur")
            recupererErreur ();

        else if( listeArguments.at (1) == "recupSansReponse")
            recupererSansReponses();
    }
}















MainWindow::~MainWindow(){

    system("taskkill /F /IM MoniteurWebService.exe");

    delete ui;
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Lancer le PDFScrute afin de chercher de nouveau fichier DPEC a envoyer
  */
void MainWindow::lancerAPI(){

    if( config.getPathNas () == "Inconnu" )
        afficherMessage(config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0001" ), false);
    else{

        Dpecscrute *dpecScrute = new Dpecscrute();

        connect (dpecScrute, SIGNAL(on_fichierEnCours(QString)), this, SLOT(fichierEnCours(QString)));

        connect (dpecScrute, SIGNAL(on_renvoyerFichier(QString,QString)), this, SLOT(renvoyerMessage(QString,QString)));

        connect (dpecScrute, SIGNAL(on_message(QString, bool)), this, SLOT(afficherMessage(QString,bool)));

        connect (this, SIGNAL(on_stop_iteration(bool)), dpecScrute, SLOT(stoperIterateur(bool)));

        QtConcurrent::run( dpecScrute, &Dpecscrute::scruter, config.getPathNas (), config.getPathRenvoi() );
    }
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lister les idChrono etant dans un etat d'attente pour obtenir une visualisation
  */
void MainWindow::lancerRelance(){

    siRelance = true;

    dateRelance = QDate::currentDate ().toString ("yyyyMMdd");

    typeAction = "visualiser";

    QList<QPair<QString,QString> > listeRelance = OBJ_bdd.getRelance (dateRelance);

    visualiserDossier ( listeRelance );
}
























//----------------------------------------------------------------------------------------------------------------------
/*
  Lance la visualiser des dossiers
  */
void MainWindow::visualiserDossier(QList<QPair<QString, QString> > listeDossier){

    afficherMessage("----------------------------", false);
    afficherMessage("Nombre de relance : " % QString::number ( listeDossier.length () ) , false);

    ui->lineEdit_nbRelance->setText ( QString::number ( listeDossier.length () ) );

    Relance *relance = new Relance();

    connect (relance, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

    connect (relance, SIGNAL(lancerVisualisation(QString,QString)), this, SLOT(visualierPEC(QString,QString)));

    connect (relance, SIGNAL(traitementFiniRelance()), this, SLOT(finRelance()));

    connect (this, SIGNAL(on_pause_relance(bool)), relance, SLOT(pauseRelance(bool)));

    connect (relance, SIGNAL(on_incrementerVisualisation(int)), this, SLOT(incrementerCompteurRelance(int)));

    QtConcurrent::run(relance, &Relance::relancer, listeDossier, PATH_DOSSIER_CONFIGURATION);
}



















//----------------------------------------------------------------------------------------------------------------------/*
/*
  Permet de generer un fichier PDF (refus/accord) via un patron HTML
  + insertion des donnees presente dans la liste
  */
void MainWindow::genererPDF(QStringList listParametre, QString rnm, QString etatRetour, QString libelleRefus){

    QString patron = lireContenu( PATH_DOSSIER_FICHIER % "/" % rnm % "_generationPdf.html");

    if( patron != "-1" ){

        Fichier *fichierPdf = new Fichier();

        connect( fichierPdf, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

        connect( fichierPdf, SIGNAL(on_reconcilierFichier(QString,QString)), this, SLOT(reconcilierMessage(QString,QString)));

        QString fluxXmlDpec = lireContenu( PATH_DOSSIER_HISTORY % "/"
                                           % getRnm( idChrono) % "/"
                                           % OBJ_bdd.getValue("dateDpec", "idChrono", idChrono) % "/"
                                           % idChrono % "_DPEC.xml");

        fichierPdf->insererDonnees( fluxXmlDpec, patron, listParametre, PATH_RETOUR_PDF, idChrono, etatRetour, libelleRefus );
    }
    else
        afficherMessage("Impossible de charger le patron HTML pour generer le fichier PDF retour", false);
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de telecharger le retour de prise en charge
  */
void MainWindow::telechargerRpec(QString rnm){

//    testTelechargerRpec(rnm);
    QString requete = creerFichierEditionPec(rnm);

    if( requete != "-1" ){

        //Preparation de la requete
        QNetworkAccessManager *m_network;
        m_network = new QNetworkAccessManager(this);

        QNetworkRequest networkReq;
        QSslConfiguration confSsl;

        //S'il y a un certificat
        if( config.getValue (PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Telechargement", "Certificat") == "1" ){

            afficherMessage("Chargement de la configuration SSL", false);

            QFile certFile(PATH_DOSSIER_CERTIFICAT % "/telechargementPec_" % rnm % ".p12");
            certFile.open(QFile::ReadOnly);
            QSslCertificate certificateClient;
            QSslKey key;
            QList<QSslCertificate> listeCertificat;

            QSslCertificate::importPkcs12(&certFile, &key, &certificateClient, &listeCertificat,
                                          config.getValue( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini","Telechargement","codePin").toLatin1() );

            confSsl = networkReq.sslConfiguration();

            confSsl.setCaCertificates(listeCertificat);
            confSsl.setLocalCertificate(certificateClient);
            confSsl.setPrivateKey(key);
            confSsl.setProtocol(QSsl::AnyProtocol);
            confSsl.setPeerVerifyMode(QSslSocket::VerifyNone);

            networkReq.setSslConfiguration(confSsl);
        }
        else{

            afficherMessage ("Pas de certificat a charger", false);

            confSsl = networkReq.sslConfiguration ();
            confSsl.setPeerVerifyMode(QSslSocket::VerifyNone);
            networkReq.setSslConfiguration(confSsl);
        }

        ecrireLog(PATH_TEMP % "/" % idChrono % "_DPEC_PDF.xml", requete, false );

        deplacerFichier(PATH_TEMP % "/" % idChrono % "_DPEC_PDF.xml",
                        PATH_DOSSIER_HISTORY % "/" % rnm % "/" %QDate::currentDate().toString("yyyyMMdd")
                        % "/" % idChrono % "_DPEC_PDF.xml", true );


        networkReq.setHeader( QNetworkRequest::ContentTypeHeader, QLatin1String("text/xml;charset=utf-8") );
        networkReq.setRawHeader("SOAPAction", config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini",
                                                                "Telechargement", "actionSoap" ).toLatin1() );
        networkReq.setUrl( QUrl ( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Telechargement", "cible" ) ) );

        afficherMessage ("Telechargement RPEC de la mutuelle " % rnm % " au format PDF en cours..", false);

        chrono->start ();

        //Lancement de la requete avec la methode POST
        m_network->post( networkReq, requete.toUtf8 ().constData () );

        //Connexion reponse
        connect ( m_network, SIGNAL( finished(QNetworkReply *) ), this, SLOT( retourFichierPec(QNetworkReply *) ) );
    }
    else
        afficherMessage("Veuillez verifier le fichier patron pour l'edition des PEC dans le dossier " % PATH_DOSSIER_FICHIER, true);
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Retour de la requete envoyer au service d'edition des pec
  */
void MainWindow::retourFichierPec(QNetworkReply *reply){

    //Retour NOK
    if (reply->error() > 0){

        afficherMessage ("Erreur lors du telechargement d'un retour de prise en charge : " % reply->errorString()
                         % " - Code erreur : " % QString::number ( reply->error () ), false);

        OBJ_bdd.majDonnee ( idChrono, "editionRpec", reply->errorString () );
    }
    else{

        int tempsChrono = chrono->elapsed();
        afficherMessage("Temps telechargement PEC : " % QString::number( tempsChrono ) % " ms", false);

        OBJ_bdd.majDonnee( idChrono, "tempsEditionPec", QString::number( tempsChrono ) );

        QByteArray retourEditionPec = reply->readAll();

        reply->deleteLater();

        ecrireLog(PATH_TEMP % "/" % idChrono % "_RPEC_PDF.xml", retourEditionPec, false );

        deplacerFichier(PATH_TEMP % "/" % idChrono % "_RPEC_PDF.xml",
                        PATH_DOSSIER_HISTORY % "/" % getRnm(idChrono) % "/"
                        % QDate::currentDate().toString("yyyyMMdd") % "/" % idChrono % "_RPEC_PDF.xml", true);

        Message *messageRpec = new Message();

        //Connexion message
        connect (messageRpec, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

        //Connexion deplacement fichier
        connect (messageRpec, SIGNAL(on_deplacerFichier(QString,QString,bool)), this, SLOT(deplacerFichier(QString,QString,bool)));

        //Connexion analyse donn�es xml + envoi 4axes.net
        connect (messageRpec, SIGNAL(on_envoyerVers4axes(QString,QString,QString,QString)), this,
                 SLOT(preparerRetourPec(QString,QString,QString,QString)));

        //Connexion forcer dpec suivante
        connect (messageRpec, SIGNAL(on_dpecSuivante()), this, SLOT(relancerScrute()));

        //Connexion fin des relances
        connect (messageRpec, SIGNAL(on_FinRenvoi()), this, SLOT(renvoiFini()));

        //Connexion pause ON / pause OFF
        connect (this, SIGNAL(on_pause_relance(bool)), messageRpec, SLOT(setPauseIteratio(bool)));

        connect(messageRpec, SIGNAL(on_decoderPec(QString,QString)), this, SLOT(decoderFlux(QString,QString)));

        //On parse le fichier retour
        QtConcurrent::run(messageRpec, &Message::parserRetourWs, idChrono, retourEditionPec, getRnm( idChrono ), QString("editionRpec") );
    }
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de decoder le flux base64 -> QString
  */
void MainWindow::decoderFlux(QString donnee, QString idChronoPec){

    if( !donnee.isEmpty () ){

        QByteArray fichierPdf;
        fichierPdf.append ( donnee );

        //On enregistre la donnee dans un fichier PDF
        if( enregistrerFichierPdf( QByteArray::fromBase64 ( fichierPdf ), PATH_RETOUR_PDF, idChronoPec ) )
            reconcilierMessage( PATH_RETOUR_PDF % "/" % idChronoPec % ".pdf", idChronoPec );

        else
            afficherMessage("Fichier PDF RPEC non enregistre dans " % PATH_RETOUR_PDF, false);
    }
    else
        afficherMessage ("Contenu PDF vide", true);
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ecrire un fichier au PDF nomme par l'IDCHRONO de la demande de prise en charge
  Une fois le fichier enregistre celui-ci sera envoye sur la plateforme pour etre reconcilie
  */
bool MainWindow::enregistrerFichierPdf(QByteArray fluxPdf, QString pathDestination, QString idChrono){

    QFile fichier( pathDestination % "/" % idChrono % ".pdf");
    if( ! fichier.open ( QIODevice::WriteOnly ) ){

        afficherMessage ("Impossible d'ouvrir le fichier " % fichier.fileName () % " -- Erreur : " % fichier.errorString (), true );

        return( false );
    }
    else{

        QDataStream flux (&fichier);
        flux << fluxPdf;
        fichier.close ();

        afficherMessage ("Fichier PDF telecharge et enregistre sur le disque", false);

        return( true );
    }
}






















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::reconcilierMessage(QString pathFichier, QString idChronoDpec){

    QFile fichier;
    if( !fichier.exists ( pathFichier ) )
        afficherMessage ("Le fichier " % pathFichier % " n'existe pas", true);

    else{

        if( !OBJ_bdd.getValue("etatTransmission", "idChrono", idChronoDpec ).isEmpty() ){

            afficherMessage("Le reponse a deja ete emise", false);

            QFileInfo fichierInfo( pathFichier );

            deplacerFichier(pathFichier, PATH_RETOUR_PDF % "/NR/" % fichierInfo.fileName(), true);

            if( receptionMail )
                emit on_pause_traitementMail( false );

            else
                emit on_stop_iteration ( false );
        }
        else{

            //Maj de la colonne lorsqu'on r�concilie un PDF
            OBJ_bdd.majDonnee( idChronoDpec, "dateRpec", QDate::currentDate().toString("yyyyMMdd") );

            QStringList listeArguments;

            afficherMessage("Envoi du fichier sur " % ADR_HTTP_RECONCILIER, false);

            listeArguments << "-k"
                           << "-H"
                           << HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                           << "--form"
                           << "id=" % idChronoDpec
                           << "--form"
                           << "rnm=1"
                           << "--form"
                           << "fichier=@" % pathFichier
                           << ADR_HTTP_RECONCILIER;

            QProcess *processus = new QProcess();
            processus->start(PATH_EXE_CURL, listeArguments);
            processus->setProcessChannelMode(QProcess::MergedChannels);
            processus->waitForFinished(-1);

            //Si sortie OK
            if( processus->exitCode () == 0 ){

                QString fluxRetour = processus->readAll ();

                chrono->start ();

                idChrono = idChronoDpec;

                pathFichierPdf = pathFichier;

                parserReponse( fluxRetour, "reconciliationPdf" );
            }
            else{

                afficherMessage("Erreur processus cUrl.exe ( " % QString::number( processus->exitCode() )
                                % " ) - " % processus->errorString(), true);

                OBJ_bdd.majDonnee ( idChrono, "etatTransmission", "Erreur retour : " % processus->errorString() );
            }
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
/*
 * R�ception du retour de r�conciliation sur 4AXES
 * */
void MainWindow::retourRetourRest(QNetworkReply *reply){

    if ( reply->error() > 0 )
        afficherMessage( "Erreur lors de l'envoi du fichier sur 4AXES.NET " % reply->errorString(), true);

    else{

        afficherMessage("Reception du retour 4AXES.NET", false);

        QByteArray data = reply->readAll();

        if ( data.length () > 0 ){

            reply->deleteLater ();

            OBJ_bdd.majDonnee( idChrono, "tempsTransmission",  QString::number ( chrono->elapsed() ) );

            parserReponse ( data, "reconciliationPdf" );
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
/*
 * Lorsque la classe "emailing" a terminer le traitement
 * */
void MainWindow::finPop(){

    if( siRelance || renvoiRpec)
        emit on_pause_relance ( false );
    else
        emit on_stop_iteration ( false );

    QFile::remove(QCoreApplication::applicationDirPath() % "/popEnCours");

    receptionMail = false;
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lancer une visualisation concernant un idChrono [idChronoDPEC]
  La date de la DPEC est important puisque gr�ce a ca nous allons pouvoir rechercher dans
  \History\rnmMutuelle\dateDpec la DPEC initiale pour creer la visulisation
  */
void MainWindow::visualierPEC(QString idChronoDPEC, QString dateDPEC){

    //Si le flux a relanc� a bien un num�ro de PEC op�rateur
    if( !OBJ_bdd.getValue("idMessage", "idChrono", idChronoDPEC).isEmpty() ){

        //Recup�re le patron de visualisation concerne
        QString patronVisulisation = lireContenu( PATH_DOSSIER_FICHIER % "/" % getRnm ( idChronoDPEC ) % "_visualisation.xml" );

        if( patronVisulisation != "-1" ){

            //On charge la DPEC initiale gr�ce au RNM et a la dateDpec
            QFile fichierXML ( PATH_DOSSIER_HISTORY % "/" % getRnm ( idChronoDPEC ) % "/" % dateDPEC % "/" % idChronoDPEC % "_DPEC.xml");
            if ( ! fichierXML.open ( QIODevice::Text | QIODevice::ReadWrite ) )
                afficherMessage("Impossible d'ouvrir la DPEC | Erreur " % fichierXML.errorString (), false);
            else{

                QTextStream fluxXML(&fichierXML);

                QString ligneCourante;
                QString colonneBDD;
                QString numeroVnet;

                QStringList listeLigne;

                //On charge la liste des balises qui doivent etre integree dans la visualisation de la PEC
                QStringList listeBaliseRelance = config.getListeCle ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm (idChronoDPEC) % "_DPEC.ini",
                                                                      "Balise_relance" );
                //On parcourt la DPEC initiale
                while( !fluxXML.atEnd () ){

                    //Lecture ligne courante
                    ligneCourante = fluxXML.readLine ();

                    //On parcourt la liste des balises concernees => groupe [Balise_relance]
                    for(int i = 0 ; i < listeBaliseRelance.length () ; i++){

                        //Si la balise est active (s=1)
                        if( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChronoDPEC ) % "_DPEC.ini", "Balise_relance",
                                              listeBaliseRelance.at (i) ) == "1" ){


                            //Si la ligne contient une balise et que celle-ci n'est pas deja dans la liste de ligne deja traite
                            if( ( ligneCourante.contains ( "<" % listeBaliseRelance.at (i) % ">") ) && !listeLigne.contains ( ligneCourante ) ){

                                colonneBDD = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChronoDPEC ) % "_DPEC.ini",
                                                                          "Balise_equivBDD_relance", listeBaliseRelance.at (i) );

                                if( colonneBDD != "-1" ){

                                    //Si la valeur de la cle est une colonne
                                    if( OBJ_bdd.siColonneExiste ( colonneBDD ) ){

                                        numeroVnet = OBJ_bdd.getValue ( colonneBDD, idChronoDPEC );
                                        ligneCourante.insert ( ligneCourante.indexOf (">") + 1, numeroVnet);
                                    }
                                    else
                                        ligneCourante = "<" % listeBaliseRelance.at (i) % ">" % colonneBDD % "</" % listeBaliseRelance.at (i) % ">";

                                }
                                listeLigne.push_back ( ligneCourante );
                            }
                        }
                    }
                }


                //Si il est necessaire d'ajouter une balise qui n'existe pas dans le flux de la DPEC (exemple : ALMERYS)
                if( config.siGroupeExiste ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChronoDPEC ) % "_DPEC.ini",
                                            "Balise_relance_insertion") ){

                    QStringList listeCle = config.getListeCle (PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChronoDPEC )
                                                               % "_DPEC.ini", "Balise_relance_insertion");

                    for(int i = 0 ; i < listeCle.length () ; i++){

                        colonneBDD = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChronoDPEC ) % "_DPEC.ini",
                                                                  "Balise_equivBDD_relance", listeCle.at (i) );
                        if( colonneBDD != "-1" ){

                            numeroVnet = OBJ_bdd.getValue ( colonneBDD, idChronoDPEC );

                            listeLigne.push_back ("<" % listeCle.at (i) % ">" % numeroVnet % "</" % listeCle.at (i) % ">");
                        }
                    }
                }

                //Fermeture fichier
                fichierXML.close ();

                //Creer le fichier de visualisation, place dans le dossier Relance du NAS
                creerFichierVisualisation( patronVisulisation.replace ("--DATA--", listeLigne.join ("\n") ), idChronoDPEC );
            }
        }
        else
            afficherMessage("Erreur lors de la recuperation du patron XML de relance", false);
    }
    else{

        afficherMessage("Aucun numero de PEC operateur pour l'IDCHRONO " % idChronoDPEC, false);

        deplacerFichier(PATH_DOSSIER_HISTORY % "/" % getRnm ( idChronoDPEC ) % "/" % dateDPEC % "/" % idChronoDPEC % "_DPEC.xml",
                        config.getPathRenvoi() % "/" % idChronoDPEC % "_DPEC.xml", false);

        OBJ_bdd.majDonnee( idChronoDPEC, "etatRpec", "aRenvoyer" );
    }

}



























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::finRelance(){

    /*
      On recup�re les statistiques d'apr�s relance
      */
    QString nbAccord = OBJ_bdd.compterEnregistrement ( "tbl_frontal", "etatRpec", "accord", "dateRelance", dateRelance );

    QString nbEnAttente = OBJ_bdd.compterEnregistrement ( "tbl_frontal", "etatRpec", "retourEnAttente", "dateRelance", dateRelance );

    QString nbRefus = OBJ_bdd.compterEnregistrement ( "tbl_frontal", "etatRpec", "refus", "dateRelance", dateRelance );

    QString nbReconicile = OBJ_bdd.compterEnregistrement ( "tbl_frontal", "etatTransmission", "reconcilie", "dateRelance",
                                                           dateRelance );

    QString nbNonReconcilie = OBJ_bdd.compterEnregistrement ( "tbl_frontal", "etatTransmission", "Non reconcilie - Statut DPEC incorrect",
                                                              "dateRelance", dateRelance );

    ecrireLog ( NOM_FICHIER_LOG_RELANCE, "Nombre d'accord : " % nbAccord, true );
    ecrireLog ( NOM_FICHIER_LOG_RELANCE, "Nombre de refus : " % nbRefus, true );
    ecrireLog ( NOM_FICHIER_LOG_RELANCE, "Nombre de reconcilie : " % nbReconicile, true );
    ecrireLog ( NOM_FICHIER_LOG_RELANCE, "Nombre de non reconcilie : " % nbNonReconcilie, true );
    ecrireLog ( NOM_FICHIER_LOG_RELANCE, "Nombre de mise en attente : " % nbEnAttente, true );

    ecrireIHM ("Fin relance");
    siRelance = false;

    QCoreApplication::quit ();
}
































//----------------------------------------------------------------------------------------------------------------------
/*
  Ecrit dans un fichier de visualisation les donnees structurees SOAP
  */
void MainWindow::creerFichierVisualisation(QString resultat, QString idChronoDPEC){

    QString pathFichier = config.getPathRelance () % "/" % idChronoDPEC % "_RELANCE.xml";

    QFile fichierVisualisation( pathFichier );
    if( ! fichierVisualisation.open ( QIODevice::Text | QIODevice::WriteOnly ) )
        afficherMessage("Impossible d'ouvrir le fichier " % idChronoDPEC % "_RELANCE.xml | Erreur : "
                        % fichierVisualisation.errorString (), true);
    else{

        QTextStream fluxVisualisation(&fichierVisualisation);

        fluxVisualisation << resultat << endl;

        fichierVisualisation.close ();

        fichierEnCours ( pathFichier );
    }
}















//----------------------------------------------------------------------------------------------------------------------
/*
 * Permet de recuperer le fichier patron pour l'appelle des PEC en PDF aupr�s du webService distant
  */
QString MainWindow::creerFichierEditionPec(QString rnmMutuelle){

    QString patron = lireContenu ( PATH_DOSSIER_FICHIER % "/" % rnmMutuelle % "_editionPec.xml" );

    if( patron != "-1"){

        QStringList listeEquivDD = config.getListeCle (PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_DPEC.ini", "Balise_quivBDD_editionPec");

        for(int i = 0 ; i < listeEquivDD.length () ; i++ ){

            patron.replace ("<" % listeEquivDD.at (i) % "></" % listeEquivDD.at (i) % ">",
                            "<" % listeEquivDD.at (i) % ">"
                            % OBJ_bdd.getValue (config.getValue (PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle
                                                                 % "_DPEC.ini", "Balise_quivBDD_editionPec", listeEquivDD.at (i)), idChrono)
                            % "</" % listeEquivDD.at (i) % ">");
        } 
    }
    return(patron);
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner le RNM de l'organisme depuis une IDCHRONO donne
  */
QString MainWindow::getRnm(QString idChronoDPEC){

    return( idChronoDPEC.split ("_").at (1) );
}































//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le patron de visulisation
  */
QString MainWindow::lireContenu(QString pathFichier){

    //On charge le fichier
    QFile fichier (pathFichier);
    if ( ! fichier.open ( QIODevice::Text | QIODevice::ReadWrite ) )
        afficherMessage ("Impossible d'ouvrir le fichier " % pathFichier % "  | Erreur " % fichier.errorString (), true);
    else{

        QTextStream flux(&fichier);

        QString contenu = flux.readAll();

        fichier.close ();

        return( contenu );
    }
    return("-1");
}





































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher le fichier en cours de traitement
  */
void MainWindow::fichierEnCours(QString nomFichier){

    afficherMessage("----------------------------------------------------------------", false);
    afficherMessage("Fichier en cours de traitement : " % nomFichier, false);

    bool erreur = false;

    fichierInfoXML.setFile ( nomFichier );
    QFile fichier;

    //Reconciliation en cours pour eviter de couper le moniteur en plein reconciliation
    reconciliationEnCours = true;

    //Selon ce que 4AXES.NET m'envoi
    if( fichierInfoXML.baseName ().contains ("creation") ){

        idChrono = fichierInfoXML.baseName ().replace ("creation","");
        typeAction = "creer";

        suffixe = "";

        //On verifie l'existance d'un idChrono dans la base de donnees
        if( OBJ_bdd.verifierDonnee ( idChrono ) ){

            afficherMessage("IDCHRONO deja traite - Fichier a deplacer dans " % PATH_ERR_ENVOI_XML, false);

            deplacerFichier ( fichierInfoXML.absoluteFilePath (), PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName (), true );

            erreur = true;
        }
    }
    else if ( fichierInfoXML.baseName ().contains ("annulation") ){

        idChrono = fichierInfoXML.baseName ().replace ("annulation","");
        typeAction = "annuler";

        //suffixe du fichier dans history
        suffixe = "_ANNULATION";

        //On verifie qu'il existe un IDCHRONO pour effectue l'annulation
        if( OBJ_bdd.verifierDonnee ( idChrono ) ){

            //On met a jour la colonne "annulation" a oui pour l'idChrono [idChrono]
            if( OBJ_bdd.majDonnee ( idChrono, "annulation", "oui") ){

                //Si le fichier doit obtenir des modifications
                if( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChrono ) % "_DPEC.ini", typeAction,
                                      "modifierFichier") == "1" )
                    modifierFichier( fichierInfoXML.absoluteFilePath (), idChrono );
            }
            else
                erreur = true;
        }
        else{

            QString libelleErreur = config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0003" );

            afficherMessage(libelleErreur, false);

            //On enregistre l'IDCHRONO dans un fichier log pour envoyer ce fichier a thomas pour extraction des donnees
            //Puis envoi des informations extraitent a l'orgaisme pour appliquer l'annulation
            ecrireLog ( NOM_FICHIER_LOG_ANNULATION, idChrono, false);

            envoyerPieceJointe ( fichierInfoXML.absoluteFilePath (), libelleErreur % " [ IDCHRONO : " % idChrono % " ]", "Annulation", NULL);

            deplacerFichier ( fichierInfoXML.absoluteFilePath (), PATH_DOSSIER_ANNULATION % "/"
                              % fichierInfoXML.fileName (), true );

            erreur = true;
        }
    }
    else if( fichierInfoXML.baseName ().contains ("RELANCE") ){

        idChrono = fichierInfoXML.baseName ().replace ("_RELANCE","");

        //Suffixe du fichier dans history
        suffixe = "_RELANCE";
        typeAction = "visualiser";
    }
    else{

        envoyerMail ("Un fichier non identife est present dans le repertoire de depart [DPEC] -- Nom du fichier : "
                     % fichierInfoXML.fileName () % " - Ce fichier sera deplace dans " % PATH_ERR_NOM_FICHIER_DPEC);

        deplacerFichier ( fichierInfoXML.absoluteFilePath (), PATH_ERR_NOM_FICHIER_DPEC
                          % "/" % fichierInfoXML.fileName (), true );

        erreur = true;
    }

    if( !erreur ){

        //On retire le contexte du fichier ID_CHRONOcontexte.xml en ID_CHRONO.xml
        if( fichier.rename ( fichierInfoXML.absoluteFilePath (),
                             fichierInfoXML.absolutePath () % "/" % idChrono % ".xml") ){

            afficherMessage ("Fichier renomme", false);

            fichierInfoXML.setFile ( fichierInfoXML.absolutePath () % "/" % idChrono % ".xml" );

            afficherMessage ("Taille fichier : " % QString::number ( fichierInfoXML.size () ) % " Ko", false );
            afficherMessage ("ID_CHRONO en cours : " % idChrono, false);

            //On recup�re le rnm de la mutuelle en cours
            rnmMutuelleEnCours = getRnm ( idChrono );

            Sleep(500);

            if( fichierInfoXML.size () != 0 )
                envoyerMessage( fichierInfoXML.absoluteFilePath (), typeAction );
            else{

                emit on_stop_iteration ( false ); //a mettre en commentaire

                reconciliationEnCours = false;
            }
        }
        else{

            afficherMessage("Impossible de renommer le fichier " % fichierInfoXML.fileName () % " | Erreur : " % fichier.errorString (), false);
            afficherMessage("Fichier a deplacer dans " % PATH_ERR_ENVOI_XML, false);

            deplacerFichier ( fichierInfoXML.absoluteFilePath (), PATH_ERR_ENVOI_XML
                              % "/" % fichierInfoXML.fileName (), true );

            OBJ_bdd.enregistrerErreur ( PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName (),
                                        "Impossible de renommer le fichier " % fichierInfoXML.fileName ()
                                        % " | Erreur : " % fichier.errorString ());

            emit on_stop_iteration ( false ); //a mettre en commentaire

            reconciliationEnCours = false;
        }
    }
    else{

        emit on_stop_iteration ( false ); //a mettre en commentaire
        reconciliationEnCours = false;
    }
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Ecrit des informations sur l'interface
  */
void MainWindow::ecrireIHM(QString info){

    QListWidgetItem *item = new QListWidgetItem( QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss") % " : " % info );
    ui->zoneTexte->insertItem( ui->zoneTexte->count() + 1, item);
    ui->zoneTexte->scrollToBottom();
}













//----------------------------------------------------------------------------------------------------------------------
void MainWindow::debugger(QString titre, QString info){

    QMessageBox::warning (NULL,titre, info);
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lancer la requete vers le webService
  */
void MainWindow::envoyerMessage(QString nomFichierXML, QString contexteAction){

    //On ajoute l'idChrono
    if( OBJ_bdd.ajouterDonnee ( idChrono, contexteAction ) ){

        Sleep(100);

        if( OBJ_bdd.majDonnee ( idChrono, "idEntite", rnmMutuelleEnCours ) ){

            Sleep(100);

            ecrireLog ( NOM_FICHIER_LOG, "Informations inserees en base de donnees", true);

            //On recup�re le contenu du fichier xml provenant de 4AXES.NET
            QString requete = lireContenu( nomFichierXML );

            //Si le nombre de tentative a atteint sa limite
            if( requete != "-1" ){

                //On recup�re l'adresse du webService
                QString adresseWebService = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelleEnCours % "_DPEC.ini", "Adresse", "cible");

                //Si l'adresse cible du webService n'a pas ete configuree
                if( adresseWebService != "-1" ){

                    ui->lineEdit_adrSite->setText ( adresseWebService );

                    //On recup�re l'action a appeler au niveau du webService
                    QString action = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelleEnCours % "_DPEC.ini", "Action", contexteAction);

                    //Si l'action du webService du webService visee n'est pas renseignee
                    if( action != "-1" ){

                        //On met a jour la colonne "action"
                        if( OBJ_bdd.majDonnee ( idChrono, "action", contexteAction ) ){

                            ui->lineEdit_action->setText ( contexteAction );

                            chrono->start ();

//                            parsertest ();

                            //Preparation de la requete
                            QNetworkAccessManager *m_network;
                            m_network = new QNetworkAccessManager(this);

                            QNetworkRequest networkReq;
                            networkReq.setHeader( QNetworkRequest::ContentTypeHeader, QLatin1String("text/xml;charset=utf-8") );
                            networkReq.setRawHeader("SOAPAction", action.toLatin1() );
                            networkReq.setUrl( QUrl ( adresseWebService ) );

                            afficherMessage("Envoi du flux vers la mutuelle " % rnmMutuelleEnCours, false);

                            m_network->post( networkReq, requete.toUtf8 ().constData () );

                            //Connexion reponse
                            connect( m_network, SIGNAL( finished(QNetworkReply *) ), this, SLOT( retourEnvoiPecWs(QNetworkReply *) ) );
                        }
                        else
                            afficherMessage( config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0012")
                                             % idChrono, false);
                    }
                    else
                        afficherMessage("Verifier la valeur la cle " % typeAction % " dans le groupe ACTION du fichier de configuration "
                                        % rnmMutuelleEnCours % "_DPEC.ini", false);
                }
                else
                    afficherMessage( config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0007")
                                     % rnmMutuelleEnCours % "_DPEC.ini", false);
            }
            else{

                afficherMessage("Erreur lors de la recuperation du fluxXML a envoyer", false);

                deplacerFichier ( fichierInfoXML.absoluteFilePath (), PATH_ERR_ENVOI_XML
                                  % "/" % fichierInfoXML.fileName (), true );

                OBJ_bdd.enregistrerErreur ( PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName ()
                                            ,"Erreur lors de la recuperation du fluxXML a envoyer - 5 tentatives echouees" );

                ecrireLog ( NOM_FICHIER_LOG, "Envoi e-mail alerte pour l'IDCHRONO " % fichierInfoXML.baseName (), true);

                envoyerMail ("Le fichier " % fichierInfoXML.fileName () % " n'a pas ete envoye -- "
                             " Erreur lors de la recuperation du fluxXML a envoyer - 5 tentatives echouees" );

                Sleep(100);

                emit on_stop_iteration ( false );
            }
        }
        else
            afficherMessage(config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0006") % idChrono, false);
    }
    else
        afficherMessage( config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0005") % idChrono, false);
}













//----------------------------------------------------------------------------------------------------------------------
/*
 * Permet de renvoyer un message depuis le dossier "Renvoi"
 * */
void MainWindow::renvoyerMessage(QString pathFichierXml, QString contexteAction){

    fichierInfoXML.setFile( pathFichierXml );
    idChrono = fichierInfoXML.baseName();
    typeAction = contexteAction;
    rnmMutuelleEnCours = getRnm( idChrono );

    //On recup�re le contenu du fichier xml provenant de 4AXES.NET
    QString requete = lireContenu( pathFichierXml );

    if( requete != "-1" ){

        //On recup�re l'adresse du webService
        QString adresseWebService = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelleEnCours % "_DPEC.ini", "Adresse", "cible");

        //Si l'adresse cible du webService n'a pas ete configuree
        if( adresseWebService != "-1" ){

            ui->lineEdit_adrSite->setText ( adresseWebService );

            //On recup�re l'action a appeler au niveau du webService
            QString action = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelleEnCours % "_DPEC.ini", "Action", contexteAction);

            //Si l'action du webService du webService visee n'est pas renseignee
            if( action != "-1" ){

                ui->lineEdit_action->setText ( action );

                chrono->start ();

                //Preparation de la requete
                QNetworkAccessManager *m_network;
                m_network = new QNetworkAccessManager(this);

                QNetworkRequest networkReq;
                networkReq.setHeader( QNetworkRequest::ContentTypeHeader, QLatin1String("text/xml;charset=utf-8") );
                networkReq.setRawHeader("SOAPAction", action.toLatin1() );
                networkReq.setUrl( QUrl ( adresseWebService ) );

                afficherMessage("Renvoi du fichier " % pathFichierXml % " vers la mutuelle " % rnmMutuelleEnCours, false);

                m_network->post( networkReq, requete.toUtf8 ().constData () );

                //Connexion reponse
                connect( m_network, SIGNAL( finished(QNetworkReply *) ), this, SLOT( retourEnvoiPecWs(QNetworkReply *) ) );
            }
            else
                afficherMessage("Verifier la valeur la cle " % typeAction % " dans le groupe ACTION du fichier de configuration "
                                % rnmMutuelleEnCours % "_DPEC.ini", false);
        }
        else
            afficherMessage( config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0007")
                             % rnmMutuelleEnCours % "_DPEC.ini", false);
    }
    else{

        afficherMessage("Erreur lors de la recuperation du fluxXML a envoyer", false);

        deplacerFichier ( fichierInfoXML.absoluteFilePath ()
                          , PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName (), true );

        OBJ_bdd.enregistrerErreur ( PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName ()
                                    ,"Erreur lors de la recuperation du fluxXML a envoyer - 5 tentatives echouees" );

        ecrireLog ( NOM_FICHIER_LOG, "Envoi e-mail alerte pour l'IDCHRONO " % fichierInfoXML.baseName (), true);

        envoyerMail ("Le fichier " % fichierInfoXML.fileName () % " n'a pas ete envoye -- "
                     " Erreur lors de la recuperation du fluxXML a envoyer - 5 tentatives echouees" );

        Sleep(100);

        emit on_stop_iteration ( false );
    }

}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Recup�re le retour du webService
  */
void MainWindow::retourEnvoiPecWs(QNetworkReply *reply){

    //Si erreur
    if (reply->error() > 0){

        afficherMessage("Erreur phase 1 : " % reply->errorString(), false);

        ecrireLog ( PATH_DOSSIER_LOG_ERREUR % "/" % fichierInfoXML.baseName () % "_erreur.log", reply->readAll(), false);

        //Deplacer le fichier une fois le fichier s'il y a eu un soucis lors de l'envoi
        deplacerFichier ( fichierInfoXML.absoluteFilePath ()
                          , PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName (), true );

        OBJ_bdd.enregistrerErreur ( PATH_ERR_ENVOI_XML % "/" % fichierInfoXML.fileName (), "Erreur envoi : " % reply->errorString());

        //On met a jour la base de donnees
        OBJ_bdd.majDonnee (idChrono, "motif", "Erreur envoi : " % reply->errorString() );

        afficherMessage ("Envoi e-mail alerte pour l'IDCHRONO " % fichierInfoXML.baseName (), false);

        envoyerMail ("Le fichier " % fichierInfoXML.fileName () % " n'a pas ete envoye -- Erreur " % reply->errorString() );

        reply->deleteLater ();

        emit on_stop_iteration ( false );
    }
    else{

        QByteArray data = reply->readAll();

        //Si le retour contient de la donnees
        if ( !data.isEmpty() ){

            QString pathSource;
            if ( typeAction == "creer" || typeAction == "annuler" )
                pathSource = config.getPathNas ();

            else if ( typeAction == "visualiser" )
                pathSource = config.getPathRelance ();

            else if( typeAction == "recreer" ){

                pathSource = config.getPathRenvoi();
                suffixe  = "_RENVOI";
            }


            int temps = chrono->elapsed ();

            afficherMessage ("Temps requete : " % QString::number ( temps ) % " ms", false);

            //Deplacer le fichier une fois le fichier utilise et une reponse obtenu
            if ( deplacerFichier ( pathSource % "/" % idChrono % ".xml",
                            PATH_DOSSIER_HISTORY % "/" % getRnm (idChrono) % "/" % dateTime.currentDateTime ().toString ("yyyyMMdd"),
                            "_DPEC" % suffixe ) ){

                //On enregistre le retour XML dans le dossier history\rnm\dateJour pour le conserver
                ecrireLog ( PATH_DOSSIER_HISTORY % "/" % getRnm ( idChrono )
                            % "/" % dateTime.currentDateTime ().toString ("yyyyMMdd") % "/" % idChrono % "_RPEC" % suffixe % ".xml",
                            data, false);

                OBJ_bdd.majDonnee ( idChrono, "tempsRetour", QString::number ( temps ) );

                Message *messageRpec = new Message();

                //Connexion message
                connect (messageRpec, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

                //Connexion deplacement fichier
                connect (messageRpec, SIGNAL(on_deplacerFichier(QString,QString,bool)), this, SLOT(deplacerFichier(QString,QString,bool)));

                //Connexion analyse donn�es xml + envoi 4axes.net
                connect (messageRpec, SIGNAL(on_envoyerVers4axes(QString,QString,QString,QString)), this,
                         SLOT(preparerRetourPec(QString,QString,QString,QString)));

                //Connexion forcer dpec suivante
                connect (messageRpec, SIGNAL(on_dpecSuivante()), this, SLOT(relancerScrute()));

                //Connexion fin des relances
                connect (messageRpec, SIGNAL(on_FinRenvoi()), this, SLOT(renvoiFini()));

                //Connexion pause ON / pause OFF
                connect (this, SIGNAL(on_pause_relance(bool)), messageRpec, SLOT(setPauseIteratio(bool)));

                //On parse le fichier retour
                QtConcurrent::run(messageRpec, &Message::parserRetourWs, idChrono, data, rnmMutuelleEnCours, QString("reponsePriseEnCharge") );
            }
            else
                afficherMessage ("Envoi OK : Echec deplacement fichier " % idChrono % ".xml", false);

        }
        else
            afficherMessage("Donnees retours vide", false);
    }
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de relancer le chrono
  */
void MainWindow::relancerScrute(){

    if( siRelance )
        emit on_pause_relance ( false );
    else
        on_stop_iteration ( false );
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'envoyer le retour [rpecXML] vers 4AXES.NET pour reconcilier la DPEC identifiee par [idChronoDpec]
  */
void MainWindow::preparerRetourPec(QString idChronoDpec, QString rpecXML, QString etat, QString libelle){

    if( etat == "accord" || etat == "refus" ){

        afficherMessage("Preparation de l'envoi du retour sur 4AXES.NET", false);

        QStringList listeValeurRetour;
        QString actionRetour;

        //Si pas de numero de PEC operateur
        if( OBJ_bdd.getValue ("idMessage", idChronoDpec ).isEmpty() ){

            if( etat == "accord" )
                envoyerMail("[ALERTE] - Accord sans numero PEC operateur -> " % idChronoDpec);

            else
                actionRetour  = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm(idChronoDpec)
                                                   % "_RPEC.ini", "actionSansIdMessage", "action" );
        }
        else
            actionRetour = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm(idChronoDpec)
                                               % "_RPEC.ini", "actionAvecIdMessage", "action" );

        //Si aucune action r�cup�rer
        if( actionRetour != "-1"){

            if( actionRetour == "popperRpec" ){

                if( OBJ_bdd.getValue("etatTransmission", "idChrono", idChronoDpec ) == "" )
                    popperRpec();
                else{

                    afficherMessage("Le reponse a deja ete emise", false);

                    if( siRelance || renvoiRpec )
                        emit on_pause_relance ( false );
                    else
                        emit on_stop_iteration ( false );
                }
            }
            else if( actionRetour == "telechargerRpec" )
                telechargerRpec( getRnm ( idChronoDpec ) );

            else if ( actionRetour == "integrerRpec" ){

                listeValeurRetour = creeerParametre(rpecXML, getRnm ( idChronoDpec ) );

                integrerRetour( idChronoDpec, listeValeurRetour, etat, libelle);
            }
            else if ( actionRetour == "genererRpec" ){

                listeValeurRetour = creeerParametre(rpecXML, getRnm ( idChronoDpec ) );

                genererPDF(listeValeurRetour, getRnm ( idChronoDpec ), etat, libelle );
            }
        }
        else
            afficherMessage("Veuillez verifier l'action a selectionner pour les retours de PEC pour la mutuelle "
                            % getRnm( idChronoDpec ), true);
    }
    else
        afficherMessage("Retour de mise en attente", false);
}
























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::integrerRetour(QString idChronoDpec, QStringList listeDonnees, QString etat, QString libelleRefus){

    QUrl parametre;
    QUrlQuery urlQuery;

    urlQuery.clear();
    parametre.clear();

    QByteArray donnees;
    donnees.append("idChrono=" % idChronoDpec % "&");
    donnees.append("etat=" % etat % "&");

    if( etat == "accord" ){

        listeDonnees.push_back ( "ZONEMEMOR=" % OBJ_bdd.getValue ("idMessage", idChronoDpec) );

        for(int i = 0 ; i < listeDonnees.length() ; i++){

            afficherMessage(listeDonnees.at(i), false);
        }

        donnees.append( "flux=" % listeDonnees.join (";") ) ;
    }
    else if( etat == "refus" ){

        afficherMessage("Libelle Refus : " % libelleRefus, false);

        donnees.append( "flux=ZONEMEMOR=" % libelleRefus);
    }

    //Preparation de la requete
    QNetworkAccessManager *m_network = new QNetworkAccessManager(this);

    QNetworkRequest request;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);
    request.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
    request.setRawHeader (HTTP_DOMAINE_4AXES.toLatin1 (), HTTP_DOMAINE_4AXES_VALEUR.toLatin1() );
    request.setUrl( QUrl ( ADR_HTTP_RETOUR_XML ) );

    chrono->start ();

    afficherMessage("Envoi du message a 4AXES.NET sur " % ADR_HTTP_RETOUR_XML, false);

    m_network->post( request, donnees );

    //Connexion reponse
    connect(m_network, SIGNAL( finished(QNetworkReply *) ), this, SLOT( retourEnvoi4Axes(QNetworkReply *) ) );
}












//----------------------------------------------------------------------------------------------------------------------
void MainWindow::popperRpec(){

    Emailing *Popper = new Emailing();

    connect(Popper, SIGNAL(on_message(QString,bool)), SLOT(afficherMessage(QString,bool)));

    connect(Popper, SIGNAL(popMailFini()), this, SLOT(finPop()));

    connect(Popper, SIGNAL(reconcilierPec(QString,QString)), this, SLOT(reconcilierMessage(QString,QString)));

    connect(this, SIGNAL(on_pause_traitementMail(bool)), Popper, SLOT(setPause(bool)));

    connect(Popper, SIGNAL(on_envoyerMail(QString)), this, SLOT(envoyerMail(QString)));

    connect(Popper, SIGNAL(on_deplacerFichier(QString,QString,bool)), this, SLOT(deplacerFichier(QString,QString,bool)));

    connect(Popper, SIGNAL(on_envoyerMailPj(QString,QString,QString)), this,
            SLOT(envoyerPieceJointe(QString,QString,QString,QString)));

    receptionMail = true;

    QtConcurrent::run(Popper, &Emailing::recupererPieceJointe, QCoreApplication::applicationDirPath() );
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  Receptionne le retour de 4AXES.NET
  */
void MainWindow::retourEnvoi4Axes(QNetworkReply *reply){

    if ( reply->error() > 0 ){

        afficherMessage(config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0008") % reply->errorString(), false);

        OBJ_bdd.majDonnee ( idChrono, "etatTransmission", "Erreur retour : " % reply->errorString() );
    }
    else {

        QByteArray data = reply->readAll();

        reply->deleteLater ();

        parserReponse ( data, "integrationXml");
    }
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Parse le retour provenant de 4AXES.NET apr�s l'envoi du retour XML de VIAMEDIS
  */
void MainWindow::parserReponse(QString reponse, QString contexte){

    //Si le retour n'est pas vide
     if( !reponse.isEmpty () ){

         //En cas de maintenance
         if( reponse.contains ( "Connexion refusee" ) )
             afficherMessage("Connexion refusee : IP non autorisee / Plateforme en maintenance / Nom de domaine errone", true);
         else{

             if( contexte == "reconciliationPdf"){

                 deplacerFichier(pathFichierPdf,
                                PATH_DOSSIER_HISTORY % "/" % getRnm( idChrono ) % "/"
                                % QDate::currentDate().toString("yyyyMMdd") % "/" % idChrono % "_PDF.pdf", true);
             }

             //Temps de requete aller-retour
            int temps = chrono->elapsed ();

            bool reconcilier = false;

            afficherMessage("Temps retour 4AXES.NET : " % QString::number ( temps ) % " ms", false);

            //Mise a jour du temps de transmission a 4axes.net en millisecondes
            OBJ_bdd.majDonnee ( idChrono, "tempsTransmission", QString::number ( temps ) );

            QXmlStreamReader xmlReader(reponse);

            QString idChronoRpec;
            QString statutRPEC;
            QString erreur;

            while( !xmlReader.atEnd () && !xmlReader.hasError () ){

                xmlReader.readNext ();

                //Si resultat est OK
                if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" ){

                    if ( xmlReader.attributes ().value ("resultat") == "1" )
                        reconcilier = true;
                }
                else if( xmlReader.isStartElement () && xmlReader.name () == "idChrono" )
                        idChronoRpec = xmlReader.readElementText ();

                else if ( xmlReader.isStartElement () && xmlReader.name () == "statutRPEC" )
                    statutRPEC = xmlReader.readElementText ();

                else if ( xmlReader.isStartElement () && xmlReader.name () == "erreur" )
                    erreur = xmlReader.readElementText ();
            }

            if( reconcilier ){

                afficherMessage("Message reconcilie - Statut RPEC : " % statutRPEC % " | ID_CHRONO : " % idChronoRpec, false);

                if( !OBJ_bdd.majDonnee ( idChrono, "etatTransmission", "reconcilie") )
                    afficherMessage(config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0010"), false);
            }
            else if ( !reconcilier ){

                afficherMessage( "Message non reconcilie : " % erreur, false );

                if( !OBJ_bdd.majDonnee ( idChrono, "etatTransmission", "Non reconcilie - " % erreur) )
                    afficherMessage(config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur", "0x0010"), false);
            }
         }

         Sleep(250);

         if( receptionMail )
             emit on_pause_traitementMail( false );
         else if( siRelance || renvoiRpec)
             emit on_pause_relance ( false );         
         else
             emit on_stop_iteration ( false );
    }
    else
         afficherMessage(config.getValue ( PATH_FICHIER_CONFIG, "Libelle_erreur","0x0009"), false);

}





















//----------------------------------------------------------------------------------------------------------------------
/*
    Permet de creer la liste des param�tres a envoyer pour integration
    Resultat en sortie : [nomColonne4AXES_1=valeur_1;nomColonne4AXES_2=valeur_2]
  */
QStringList MainWindow::creeerParametre(QString retourXml, QString rnmMutuelle){

    Configuration *config = new Configuration();

    //On recup�re une liste contenant les cles du groupe [EquivBDD_4axes]
    QStringList listeEquivCle4axes = config->getListeCle(PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini", "EquivBDD_4axes");

    //Variable retournee
    QStringList listeResultat;

    //Liste contenant [<cle1=valeurDefaut1>; <cle2=valeurDefaut2>]
    //Exemple : [<CP=non>;<FJ=non>]
    QList<QPair<QString, QString> > listePaireEquivBDD;

    for(int i = 0 ; i < listeEquivCle4axes.length () ; i++){

       QPair<QString, QString> paire;
       paire.first = listeEquivCle4axes.at (i);

       //On insere la valeur par defaut "non"
       paire.second = "non";

       listePaireEquivBDD.push_back ( paire );
    }

    QXmlStreamReader xmlReader(retourXml);

    QString valeur;                 //Valeur entre la balise ouvrante et fermante du flux XML
    QString typeActe;               //Type de l'acte
    QString cle4AXES;               //Nom du groupe dans le fichier .out de 4AXES.NET
    QString valeurFiltre;           //Valeur de cle dans le fichier de configuration RPEC de la mutuelle au niveau du frontal
    QString balise;                 //Nom de balise XML

    //Tantqu'on est pas arrive a la fin du fichier XML
    while( !xmlReader.atEnd () && !xmlReader.hasError () ){

        //Lecture de la balise
        xmlReader.readNext ();

        //Memorisation de la balise
        balise = xmlReader.name ().toString ();

        //Si debut de balise
        if( xmlReader.isStartElement () ){

            //Si la balise existe dans le groupe [Balise_active]
            if( config->siCleExiste ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini", "Balise_active", balise ) ){

                //Si la balise [Balise_active] est a besoin d'etre lu ( = 1 )
                if( config->getValue (  PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini", "Balise_active", balise ) == "1" ){

                    //On converti les caract�res accentues
//                    valeur = QString::fromUtf8 ( xmlReader.readElementText ().toLatin1().constData () );
                    valeur = xmlReader.readElementText ();

                    //Paire de donnee -> <colonneBDD4axes;valeur>
                    QPair<QString,QString> paire;

                    //Si la liste des equivalences bdd 4axes contient le nom de la balise
                    if( listeEquivCle4axes.contains ( balise ) ){

                        paire.first = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini"
                                                         , "EquivBDD_4axes", balise );

                        paire.second = valeur;

                        if( !paire.second.isEmpty () )
                            listeResultat.push_back ( paire.first % "=" % paire.second );
                    }
                    //Si la liste des equivalences BDD 4axes contient la valeur
                    else if( listeEquivCle4axes.contains ( valeur ) ){

                        //On parcourt la liste de paire [EquivBDD_4axes;oui/non]
                        for(int i = 0 ; i < listePaireEquivBDD.length () ; i++){

                            if( listePaireEquivBDD.at (i).first == valeur ){

                                //Exemple :
                                //<CP,non> devient <CPONR,non>
                                listePaireEquivBDD[i].first = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini"
                                                                                  , "EquivBDD_4axes", valeur );

                                //Exemple :
                                //<CPONR,non> devient <CPONR,oui>
                                //On remplace la valeur par defaut par "oui"
                                listePaireEquivBDD[i].second = "oui";

                                typeActe = valeur;

                                paire.first = listePaireEquivBDD[i].first;
                                paire.second = listePaireEquivBDD[i].second;

                                if( !paire.second.isEmpty () )
                                    listeResultat.push_back ( listePaireEquivBDD[i].first % "=" % listePaireEquivBDD[i].second );
                            }
                        }
                    }
                    else{

                        if( config->siCleExiste ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini", typeActe, balise ) ){

                            //On recupere la colonne
                            cle4AXES = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnmMutuelle % "_RPEC.ini", typeActe, balise );

                            paire.first = cle4AXES;
                            paire.second = valeur;

                            if( !paire.second.isEmpty () && paire.second != "0" && paire.second != "0.0" && paire.second != "0%" && paire.second != "0 %")
                                listeResultat.push_back ( paire.first % "=" % paire.second );
                        }
                    }
                }
            }
        }
    }

    delete config;

    //On retour le resultat pour l'envoyer a 4AXES.NET sous forme [cle=valeur | cle=valeur | cle=valeur]
    return( listeResultat );
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de modifier un fichier xml
  */
void MainWindow::modifierFichier(QString pathFichierXml, QString idChrono){

    QFile fichierXml( pathFichierXml );
    if( ! fichierXml.open ( QIODevice::Text | QIODevice::ReadWrite ) )
        afficherMessage("Erreur lors de l'ouverture du fichier " % pathFichierXml % " | Erreur : " % fichierXml.errorString (), false);

    else{

        //On recup�re toutes les balises dont la valeur a besoin d'etre ajoutee ou modifiee
        QStringList listeBaliseEquiv = config.getListeCle ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChrono) % "_DPEC.ini",
                                                            "Balise_equivBDD_annulation");

        QTextStream fluxXml(&fichierXml);
        QString ligneCourante;

        QStringList listeLigne;

        //Tant qu'on est pas arrive a la fin du fichier
        while( ! fluxXml.atEnd () ){

            //Lire la ligne
            ligneCourante = fluxXml.readLine ();

            //On parcourt la liste de cle (nom de balise)
            for(int i = 0; i < listeBaliseEquiv.length () ; i++){

                //Si la ligne courante contient la balise concernee
                if( ligneCourante.contains ( "<" % listeBaliseEquiv.at (i) % ">" ) ){

                    //On ecrase la ligne <balise>nouvelleValeur</balise>
                    ligneCourante = "<" % listeBaliseEquiv.at (i) % ">"
                            % OBJ_bdd.getValue ( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm ( idChrono) % "_DPEC.ini",
                                                                   "Balise_equivBDD_annulation", listeBaliseEquiv.at (i) ), idChrono)
                            % "</" % listeBaliseEquiv.at (i) % ">";

                }
            }
            listeLigne.push_back ( ligneCourante );
        }

        fichierXml.close ();


        //TODO -> Trouver une methode permettant d'ecraser les donnees du fichier sans le reouvrir avec cette methode
        QFile fichierXmlModifier( pathFichierXml );
        if( ! fichierXmlModifier.open ( QIODevice::Text | QIODevice::WriteOnly ) )
            afficherMessage("Erreur lors de l'ouverture du fichier " % pathFichierXml % " | Erreur : " % fichierXml.errorString (), false);
        else{

            QTextStream fluxXmlModifier(&fichierXmlModifier);

            fluxXmlModifier << listeLigne.join ("\n");

            fichierXmlModifier.close ();
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::parsertest(){

    QFile fichierRetourTest( QCoreApplication::applicationDirPath () % "/retour.xml");
    if( ! fichierRetourTest.open ( QIODevice::Text | QIODevice::ReadWrite ) )
        ecrireIHM ("Erreur ouverture fichier retour test | Erreur : " % fichierRetourTest.errorString () );

    else{

        QTextStream fluxRetourTest(&fichierRetourTest);

        QString flux = fluxRetourTest.readAll ();

        int temps = chrono->elapsed ();

        afficherMessage("Temps retour webService : " % QString::number ( temps ) % " ms", false);

        OBJ_bdd.majDonnee ( idChrono, "tempsRetour", QString::number ( temps ) );

        fichierRetourTest.close ();

        Message *messageRpec = new Message();

        connect (messageRpec, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

        connect (messageRpec, SIGNAL(on_erreur(QString)), this, SLOT(receptionMessage(QString)));

        connect (messageRpec, SIGNAL(on_envoyerVers4axes(QString,QString,QString,QString)), this,
                 SLOT(preparerRetourPec(QString,QString,QString,QString)));

        connect (messageRpec, SIGNAL(on_dpecSuivante()), this, SLOT(relancerScrute()));

        connect (messageRpec, SIGNAL(on_FinRenvoi()), this, SLOT(renvoiFini()));

        connect (this, SIGNAL(on_pause_relance(bool)), messageRpec, SLOT(setPauseIteratio(bool)));

        connect (messageRpec, SIGNAL(on_decoderPec(QString,QString)), this, SLOT(decoderFlux(QString,QString)));

        //On parse le fichier retour
        QtConcurrent::run(messageRpec, &Message::parserRetourWs, idChrono, flux, rnmMutuelleEnCours, QString("reponsePriseEnCharge") );


    }
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Test : telecharger le flux contenant le fichier PDF
  */
void MainWindow::testTelechargerRpec(QString rnm){


    QFile fichier( QCoreApplication::applicationDirPath () % "/rpecRetour.xml");
    if( ! fichier.open ( QIODevice::Text | QIODevice::ReadWrite ) )
        afficherMessage ("Impossible d'ouvrir le fichier " % fichier.fileName () % " -- Erreur : " % fichier.errorString (), true );
    else{

        QTextStream flux(&fichier);

        QString data = flux.readAll ();

        fichier.close ();

        QXmlQuery queryXml;

        if( queryXml.setFocus ( data ) ){

            QString requete = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Telechargement", "fichier" );

            QString valeur;

            queryXml.setQuery ( requete );
            queryXml.evaluateTo (&valeur);

            valeur.replace ("\n","");

            //Si il n'y pas de contenu a recuperer -> erreur
            if( valeur.isEmpty () ){

                requete = config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Telechargement", "messageErreur" );

                queryXml.setQuery ( requete );
                queryXml.evaluateTo (&valeur);

                OBJ_bdd.majDonnee ( idChrono, "editionRpec", "NOK : " % valeur);

                afficherMessage ("Incident lors de la recuperation du fichier PDF : " % valeur, false);
            }
            else{

                OBJ_bdd.majDonnee ( idChrono, "editionRpec", "OK");

                decoderFlux (valeur, idChrono);
            }
        }
        else
            afficherMessage ("Erreur focus dans l'edition des pec", true);
    }
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'envoyer un e-mail
  */
void MainWindow::envoyerMail(QString message){

    QString expediteur = config.getValue ( QCoreApplication::applicationDirPath () % "/config.ini", "Emailing", "From");
    QString destinataire = config.getValue ( QCoreApplication::applicationDirPath () % "/config.ini", "Emailing", "To");

    QProcess *process = new QProcess();

    QStringList arguments;
    arguments << "-host:mail.4axes.fr" << "-from:" % expediteur << "-to:" % destinataire << "-s:Alerte frontal WS" << "-msg:" % message;

    process->execute (QCoreApplication::applicationDirPath () % "/Postie/postie.exe", arguments);

    if( process->exitCode () !=  0 )
        afficherMessage("Impossible d'envoyer l'e-mail (exitCode : " % QString::number ( process->exitCode ())
                        % ") -- Erreur : " % process->errorString (), false);
    else
        afficherMessage("E-mail envoye avec succes", false);

}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'envoyer un fichier par e-mail
  */
void MainWindow::envoyerPieceJointe(QString pathFichierLog, QString message, QString sujet, QString to){

    QString expediteur = config.getValue ( PATH_FICHIER_CONFIG, "Emailing", "From");

    QString destinataire;

    if( destinataire.isNull() )
        destinataire = config.getValue ( PATH_FICHIER_CONFIG, "Emailing", "To");
    else
        destinataire = to;

    //On verifie que la PJ existe avant d'envoyer
    if( QFile::exists ( pathFichierLog ) ){

        QProcess *process = new QProcess();
        QStringList arguments;

        //Creation des arguments
        arguments << "-host:mail.4axes.fr"
                  << "-from:" % expediteur
                  << "-to:" % destinataire
                  << "-s:" % sujet
                  << "-msg:" % message
                  << "-a:" % pathFichierLog;

        process->execute (QCoreApplication::applicationDirPath () % "/Postie/postie.exe", arguments);
        process->waitForFinished (-1);

        if( process->exitCode () !=  0 )
            afficherMessage( "Impossible d'envoyer l'e-mail (+PJ) (exitCode : " % QString::number ( process->exitCode ())
                             % ") -- Erreur : " % process->errorString (), false);
        else
            afficherMessage("E-mail envoye avec succes", false);

        delete process;
    }
    else
        afficherMessage("Aucune pi�ce jointe a envoyer", false);


}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de recuperer les flux xml aller ayant eu un echec lors de l'echec
  */
void MainWindow::recupererErreur(){

    ecrireLog ( NOM_FICHIER_LOG, "Recuperation des erreurs", true);
    ecrireIHM ( "Recuperation des erreurs" );

    QDirIterator iterateur( PATH_ERR_ENVOI_XML, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    QFile fichierXML;
    QFileInfo fichierInfo;

    QString idChronoXMLErreur;

    QStringList listeStatut;
    listeStatut << "accord" << "refus" << "retourEnAttente";

    QStringList listeIdChrono;

    while( iterateur.hasNext () ){

        fichierInfo = iterateur.next ();

        if( !fichierInfo.fileName ().contains ("DEJA_TRAITE") && fichierInfo.fileName ().contains ("creation")){

            idChronoXMLErreur = fichierInfo.baseName ().replace ("creation","");

            //Si un idChrono existe deja dans la base de donnees et que le statut est correct alors on supprimer simplement le fichier
            if( OBJ_bdd.verifierDonnee ( idChronoXMLErreur ) && listeStatut.contains ( OBJ_bdd.getValue ("etatRpec", idChronoXMLErreur) ) ){

                listeIdChrono.push_back ( idChronoXMLErreur );

                afficherMessage(idChronoXMLErreur % " deja traite", false);

                if( fichierXML.rename ( fichierInfo.absoluteFilePath (), fichierInfo.absoluteFilePath ().append ("DEJA_TRAITE.xml")) )
                    afficherMessage(idChronoXMLErreur % " renomme", false);

            }
            else if( OBJ_bdd.verifierDonnee ( idChronoXMLErreur ) && !listeStatut.contains ( OBJ_bdd.getValue ("etatRpec", idChronoXMLErreur) ) ){

                listeIdChrono.push_back ( idChronoXMLErreur );

                ecrireLog ( NOM_FICHIER_LOG, "IDCHRONO " % idChronoXMLErreur % " non envoye - Suppresion de l'enregistrement"
                            " en base de donnees", true);

                //On supprime l'enregistrement pour pouvoir renvoyer le flux
                if( OBJ_bdd.supprimerEnregistrement ("idChrono", idChronoXMLErreur ) ){

                    afficherMessage( idChronoXMLErreur % " supprime de la base de donnees", false);

                    //On deplace le fichier dans le repertoire de lancement
                    deplacerFichier ( fichierInfo.absoluteFilePath ()
                                      , config.getPathNas () % "/" % fichierInfo.fileName (), true );

                }
            }
            else if ( !OBJ_bdd.verifierDonnee ( idChronoXMLErreur ) ){


                afficherMessage( "Aucun enregistrement pour l'IDCHRONO " % idChronoXMLErreur, false);

                listeIdChrono.push_back ( idChronoXMLErreur );

                //On deplace le fichier dans le repertoire de lancement
                deplacerFichier ( fichierInfo.absoluteFilePath ()
                                  , config.getPathNas () % "/" % fichierInfo.fileName (), true );
            }
        }
    }


    /*
      SECONDE PASSE POUR LES FICHIERS SANS SUFFIXE
      */

    QDirIterator iterateur_repasse( PATH_ERR_ENVOI_XML, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    while( iterateur_repasse.hasNext () ){

        fichierInfo = iterateur_repasse.next ();

        if( !fichierInfo.fileName ().contains ("DEJA_TRAITE") && !fichierInfo.fileName ().contains ("creation")){

            idChronoXMLErreur = fichierInfo.baseName ();

            if( !listeIdChrono.contains ( idChronoXMLErreur ) )
                envoyerPieceJointe ( fichierInfo.absoluteFilePath (), "Le flux en PJ n'a jamais ete emis", "Anomalie", NULL );
            else{

                if( fichierXML.rename ( fichierInfo.absoluteFilePath (), fichierInfo.absoluteFilePath ().append ("DEJA_TRAITE.xml") ) )
                    afficherMessage("Fichier " % fichierInfo.fileName () % " renomme", false);
            }
        }
    }

    ecrireIHM ("Fin recuperation erreur");
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::lancerArchivage(){

    debugger ("","En cours de developpement");
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de recuperer les idMessages ayant eu N visualisation
  */
void MainWindow::recupererSansReponses(){

    QStringList liste = OBJ_bdd.getDossierSansReponse (10);
    QString nomFichier = QCoreApplication::applicationDirPath () % "/dossierSansReponse_" % QDate::currentDate ().toString ("yyyyMMdd") % ".log";

    if( !liste.isEmpty () ){

        ecrireLog (nomFichier, liste.join ("\n"), false);

        envoyerPieceJointe ( nomFichier, "", "Dossiers sans reponse", NULL );
    }
}

























































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de deplacer un fichier [nomFichier] dans un repertoire de destination [repDestination] avec un sens indique [sens]
  */
bool MainWindow::deplacerFichier(QString nomFichier, QString repDestination, QString sens){

    //Verifier que toutes les informations sont presentes
    if( !nomFichier.isEmpty () && !repDestination.isEmpty () ){

        //Info du fichier a traiter
        QFileInfo fichierInfo(nomFichier);

        QDir dossier;
        QString nomFichierACopier = fichierInfo.baseName () % sens % ".xml";

        bool flagDossier = true;

        //Si le dossier existe
        if( !dossier.exists ( repDestination ) ){

            //Creation du repertoire /History/RNM/dateJour
            if ( !dossier.mkpath ( repDestination ) ){

                ecrireLog (NOM_FICHIER_LOG,"Impossible de creer le dossier " % repDestination, true);
                flagDossier = false;
            }
        }

        //Si le dossier de destination existe bien
        if( flagDossier ){

            QFile fichier;

            //Si un fichier dans le repertoire existe deja renomme le fichier arrivant
            if( fichier.exists ( repDestination % "/" % nomFichierACopier ) )
                nomFichierACopier.append ( "_new_" % dateTime.currentDateTime ().toString ("yyyyMMdd_hh_mm_ss") );

            //Si la copie est impossible
            if( !fichier.copy ( fichierInfo.filePath (), repDestination % "/" % nomFichierACopier ) ){

                afficherMessage( "Impossible de copier le fichier "
                                 % nomFichierACopier % " -> " % fichier.errorString (), false);
                return( false );
            }
            else{

                //Si la suppression apr�s copie est impossible
                if( !fichier.remove ( fichierInfo.absoluteFilePath () ) ){

                    afficherMessage("Impossible de supprimer le fichier "% nomFichier % " -> " % fichier.errorString (), false);
                    return( false );
                }
                else{

                    ecrireLog ( NOM_FICHIER_LOG, "Fichier deplace dans " % repDestination, true );
                    return( true );
                }
            }
        }
        else{

            afficherMessage("Impossible de deplacer le fichier " % nomFichier
                            % " - Le dossier " % repDestination % " n'a pas pu etre cree", false);

            return( false );
        }
    }
    else{

        //Si le nom fichier vide
        if( nomFichier.isEmpty () )
            afficherMessage("Nom du fichier manquant", false);

        else if( repDestination.isEmpty () ) // Si nom repertoire vide
            afficherMessage("Nom du repertoire des erreurs d'envoi manquant", false);

        return( false );
    }
}


























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::deplacerFichier(QString pathFichierSource, QString pathFichierDestination, bool couperFichier){

    bool flag = true;

    QFile fichier;

    QFileInfo fichierInfo;
    fichierInfo.setFile ( pathFichierDestination );

    QDir dossierDestination;
    dossierDestination.setPath ( fichierInfo.absolutePath () );

    //On verifie que le dossier de destination existe bien
    if( !dossierDestination.exists () ){

        //S'il la creaton a echoue
        if( !dossierDestination.mkpath ( fichierInfo.absolutePath () ) )
            flag = false;
    }

    if( flag ){

        //On test si un fichier portant le meme nom existe dans le repertoire destination
        if( fichier.exists ( pathFichierDestination) ){

            //Renommer le fichier
            if( !fichier.rename ( pathFichierDestination, pathFichierDestination
                            % "_" % QDateTime::currentDateTime ().toString ("yyyyMMddhhmmss") ) ){

                afficherMessage("Impossible de renommer le fichier " % pathFichierDestination
                                % " - Erreur : " % fichier.errorString (), false);
                flag = false;
            }
        }

        if ( flag ){

            //Copie du fichier
            if( fichier.copy ( pathFichierSource, pathFichierDestination) ){

                if( couperFichier ){

                    //Suppression du fichier d'origine
                    if( !fichier.remove ( pathFichierSource ) ){

                        ecrireLog ( NOM_FICHIER_LOG, "Impossible de supprimer le fichier " % pathFichierSource, true );
                        ecrireIHM ( "Impossible de supprimer le fichier " % pathFichierSource );
                    }
                    else
                        ecrireLog ( NOM_FICHIER_LOG, "Fichier deplace dans " % pathFichierDestination, true );
                }
                else
                    ecrireLog ( NOM_FICHIER_LOG, "Fichier deplace dans " % pathFichierDestination, true );
            }
            else
                afficherMessage( "Fichier non deplace dans " % pathFichierDestination
                                % " - Erreur : " % fichier.errorString (), false );

        }
    }
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de charger le log general dans l'IHM
  */
void MainWindow::chargerLog(){

    QFile fichierLog( NOM_FICHIER_LOG );
    if( ! fichierLog.open ( QIODevice::Text | QIODevice::ReadOnly ) )
        afficherMessage( "Erreur ouverture fichier log - Erreur : " % fichierLog.errorString (), true );
    else{

        QTextStream fluxLog(&fichierLog);

        textEdit->setPlainText ( fluxLog.readAll () );

        fichierLog.close ();
    }
}






























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ecrire les logs
  */
void MainWindow::ecrireLog (QString pathFichier, QString info, bool horodatage){

    QFile fichierLog( pathFichier );
    if( !fichierLog.open ( QIODevice::Text | QIODevice::ReadWrite | QIODevice::Append ) )
        afficherMessage("Impossible d'ouvrir le fichier " % pathFichier % " | Erreur : " % fichierLog.errorString (), true );
    else{

        QTextStream fluxLog(&fichierLog);

        if( horodatage )
            fluxLog << dateTime.currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss") % " - " << info << endl;
        else
            fluxLog << info << endl;

        fichierLog.close ();
    }
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de generer un rapport sur les erreurs rencontres pour une date donnees ou une periode donnees
  */
void MainWindow::on_actionRapport_d_erreur_triggered(){

    debugger ("", "En cours de developpement");
}















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionStatistiques_triggered(){

    Fen_statistique *stats = new Fen_statistique();
    connect(stats, SIGNAL(on_message(QString)), this, SLOT(popUp(QString)));
    stats->show ();
}














//----------------------------------------------------------------------------------------------------------------------
void MainWindow::popUp(QString message){

    QMessageBox::warning (NULL,"Message", message);
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de re-analyser une RPEC et de l'envoyer sur 4axes.net
  */
void MainWindow::on_actionEnvoyer_RPEC_triggered(){

    int resultat = QMessageBox::question (NULL,"Confirmation","Fichier PDF ou fichier XML ?",
                                          QMessageBox::Yes | QMessageBox::No);

    //Si l'utilisateur confirme
    if( resultat == QMessageBox::Yes ){

        //Ouverture de la bo�te de dialogue
        QString pathFichierRPEC = QFileDialog::getOpenFileName(this, "fichier",QCoreApplication::applicationDirPath () % "/retourPDF",
                                                          "All Files (*.pdf)");
        if( !pathFichierRPEC.isEmpty() ){

            renvoiRpec = true;

            QFileInfo fichierInfo( pathFichierRPEC );

            QString idChrono =  fichierInfo.baseName();

            if( idChrono.contains("_PDF") )
                idChrono.replace("_PDF","");

            afficherMessage("--------------------------------------", false);
            afficherMessage("Traitement de l'IDCHRONO " % idChrono, false);

            reconcilierMessage( pathFichierRPEC, idChrono );
        }
    }
    else{

        resultat = QMessageBox::question (NULL,"Confirmation","Souhaitez-vous envoyer plusieurs fichiers ?",
                                              QMessageBox::Yes | QMessageBox::No);

        Message *messageRpec = new Message();

        connect (messageRpec, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

        connect (messageRpec, SIGNAL(on_envoyerVers4axes(QString,QString,QString,QString)), this,
                 SLOT(preparerRetourPec(QString,QString,QString,QString)));

        connect (messageRpec, SIGNAL(on_dpecSuivante()), this, SLOT(relancerScrute()));

        connect (messageRpec, SIGNAL(on_FinRenvoi()), this, SLOT(renvoiFini()));

        connect (this, SIGNAL(on_pause_relance(bool)), messageRpec, SLOT(setPauseIteratio(bool)));

        connect (messageRpec, SIGNAL(on_decoderPec(QString,QString)), this, SLOT(decoderFlux(QString,QString)));

        //Si l'utilisateur confirme
        if( resultat == QMessageBox::Yes ){

            //Ouverture de la bo�te de dialogue
            QString pathDossier = QFileDialog::getExistingDirectory (this, "Dossier", QCoreApplication::applicationDirPath ());

            if( !pathDossier.isEmpty() ){

                renvoiRpec = true;

                QtConcurrent::run(messageRpec, &Message::parserListeReponse, pathDossier);
            }
        }
        else{

            //Ouverture de la bo�te de dialogue
            QString pathFichierRPEC = QFileDialog::getOpenFileName(this, "fichier",QCoreApplication::applicationDirPath (),
                                                              "All Files (*.*)");
            if( !pathFichierRPEC.isEmpty()){

                QFileInfo fichierInfoRPEC(pathFichierRPEC);

                if( fichierInfoRPEC.baseName ().contains ("_RPEC_RELANCE") )
                    idChrono = fichierInfoRPEC.baseName ().replace ("_RPEC_RELANCE","");

                else if( fichierInfoRPEC.baseName ().contains ("_RPEC") )
                    idChrono = fichierInfoRPEC.baseName ().replace ("_RPEC","");


                ecrireIHM ("IDCHRONO : " % idChrono);

                QFile fichierRPEC( pathFichierRPEC );
                if( ! fichierRPEC.open ( QIODevice::Text | QIODevice::ReadWrite ) )
                    afficherMessage("Erreur ouverture fichier PREC - Erreur : " % fichierRPEC.errorString (), true );
                else{

                    QTextStream fluxRPEC(&fichierRPEC);

                    QString xml = fluxRPEC.readAll ();

                    fichierRPEC.close ();

                    afficherMessage("-----------------------", false);

                    QtConcurrent::run(messageRpec, &Message::parserRetourWs, idChrono, xml, getRnm ( idChrono ), QString("reponsePriseEnCharge") );
                }
            }
        }
    }
}

























































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lancer un flux de consultation manuellement
  */
void MainWindow::on_actionConsulter_RPEC_triggered(){

    int resultat = QMessageBox::question (NULL,"Confirmation consultation RPEC","Souhaitez-vous envoyer plusieurs fichiers ?",
                                          QMessageBox::Yes | QMessageBox::No);

    QList<QPair<QString,QString> > listePaire;
    QPair<QString,QString> paire;

    if( resultat == QMessageBox::Yes ){

        //Ouverture de la bo�te de dialogue
        QString pathDossier = QFileDialog::getExistingDirectory (this, "Dossier", QCoreApplication::applicationDirPath () );

        if( !pathDossier.isEmpty() ){

            siRelance = true;

            QDirIterator iterateur( pathDossier, QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot );

            QFileInfo fichierInfo;

            //Tant qu'il y a des fichiers dans le dossier selectionne
            while( iterateur.hasNext () ){

                //On it�re
                fichierInfo = iterateur.next ();

                //On recup�re seulement IDCHRONO
                if( fichierInfo.baseName ().contains ("_DPEC") )
                    idChrono = fichierInfo.baseName ().replace ("_DPEC","");

                else if( fichierInfo.baseName ().contains ("_RELANCE") )
                    idChrono = fichierInfo.baseName ().replace ("_RELANCE","");

                else
                    idChrono = fichierInfo.baseName ();

                //On verifie que l'IDCHRONO existe en base de donnees
                if( OBJ_bdd.verifierDonnee ( idChrono ) ){

                    //On enregistre les informations pour constituer la liste
                    paire.first = idChrono;
                    paire.second = OBJ_bdd.getValue ("dateDpec", idChrono);

                    //On verifie que la colonne "dateDpec" correspondant a l'idChrono n'est pas vide
                    if( !paire.second.isEmpty () ){

                        listePaire.push_back ( paire );

                        afficherMessage("Mise en memoire de l'IDCHRONO " % idChrono % " [ Date DPEC : "
                                        % paire.second % " ]", false);

                        paire.first.clear ();
                        paire.second.clear ();
                    }
                    else
                        afficherMessage("L'IDCHRONO " % idChrono % " n'a pas de date de depart (dateDpec) - "
                                        "Impossible de visualiser ce dossier", false);

                }
                else
                    afficherMessage("L'IDCHRONO " % idChrono % " n'existe pas dans la base de donnees - "
                                    "Impossible de visualiser ce dossier", false);
            }

            //Lancement de la visualisation
            visualiserDossier ( listePaire );
        }
    }
    else{

        //Ouverture de la bo�te de dialogue
        QString pathFichierRPEC = QFileDialog::getOpenFileName(this, "fichier",QCoreApplication::applicationDirPath (),
                                                          "All Files (*.*)");
        if( !pathFichierRPEC.isEmpty() ){

            QFileInfo fichierInfoRPEC(pathFichierRPEC);

            if( fichierInfoRPEC.baseName ().contains ("_DPEC") )
                idChrono = fichierInfoRPEC.baseName ().replace ("_DPEC","");

            else if( fichierInfoRPEC.baseName ().contains ("_RELANCE") )
                idChrono = fichierInfoRPEC.baseName ().replace ("_RELANCE","");

            else
                idChrono = fichierInfoRPEC.baseName ();

            ecrireIHM ("IDCHRONO : " % idChrono);

            if( OBJ_bdd.verifierDonnee ( idChrono ) ){

                //On enregistre les informations pour constituer la liste
                paire.first = idChrono;
                paire.second = OBJ_bdd.getValue ("dateDpec", idChrono);

                //On verifie que la colonne "dateDpec" correspondant a l'idChrono n'est pas vide
                if( !paire.second.isEmpty () ){

                    listePaire.push_back ( paire );

                    afficherMessage("Mise en memoire de l'IDCHRONO " % idChrono % " [ Date DPEC : " % paire.second % " ]",
                                    false);

                    paire.first.clear ();
                    paire.second.clear ();

                    //Lancement de la visualisation
                    visualiserDossier ( listePaire );
                }
                else
                    afficherMessage("L'IDCHRONO " % idChrono % " n'a pas de date de depart (dateDpec) -"
                                    "Impossible de visualiser ce dossier", false);
            }
            else
                afficherMessage("L'IDCHRONO " % idChrono % " n'existe pas dans la base de donnees - "
                                "Impossible de visualiser ce dossier", false);
        }
    }
}



















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::renvoiFini(){

    ecrireLog ( NOM_FICHIER_LOG, "Renvoi RPEC termine", true);
    ecrireIHM ("Renvoi RPEC termine");

    renvoiRpec = false;
}
















//----------------------------------------------------------------------------------------------------------------------
/*
 * IHM -> permet � l'utilisateur de r�cup�rer la liste des refus dans un tableau selon une p�riode ou une journ�e et selon
 * une mutuelle (RBNM)
 * */
void MainWindow::on_actionStatistique_refus_triggered(){

    //requete = SELECT count(*) as nbRefus, motif from tbl_frontal WHERE etatRpec='refus' GROUP BY motif ORDER BY nbRefus DESC

    QStringList header;
    header << "Occurence" << "Libelle Refus";

    Form_stats *stats = new Form_stats(header, "refus");
    connect (stats, SIGNAL(on_message(QString)), this, SLOT(popUp(QString)));
    stats->show ();
}

















//----------------------------------------------------------------------------------------------------------------------
/*
 * Envoi d'un e-mail pour tester la fonctionnalit�
  */
void MainWindow::on_actionTest_envoi_mail_triggered(){

    ecrireIHM ("Envoi e-mail test");

    QProcess *process = new QProcess();

    QStringList arguments;
    arguments << "-host:mail.4axes.fr" << "-from:vincent.parramon@4axes.fr" << "-to:vincent.parramon@4axes.fr" << "-s:TEST" << "-msg:TEST TEST";

    process->execute (QCoreApplication::applicationDirPath () % "/Postie/postie.exe", arguments);

    if( process->exitCode () !=  0 )
        afficherMessage( "Impossible d'envoyer l'e-mail (exitCode : " % QString::number ( process->exitCode ())
                         % " -- Erreur : " % process->errorString (), false );

}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Affichage du widget avec le calendrier incorpore
  */
void MainWindow::on_actionRelancer_depuis_une_date_triggered(){

    QWidget *widget = new QWidget();

    QCalendarWidget * calendrier = new QCalendarWidget( widget );
    connect( calendrier, SIGNAL(clicked(QDate)), this, SLOT(selectionDateRelance(QDate)));
    widget->setFixedSize( 250, 200 );
    calendrier->setFixedSize (250,200);
    widget->show();
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur une date de relance
  */
void MainWindow::selectionDateRelance(QDate date){

    int reponse = QMessageBox::question (NULL,"Confirmation","Souhaitez-vous effectuer une relance sur les dossiers "
                                         "anterieurs au " % date.toString ("dd/MM/yyyy"), QMessageBox::Yes | QMessageBox::No );

    //Si confirmation
    if( reponse == QMessageBox::Yes ){

        siRelance = true;

        dateRelance = date.toString ("yyyyMMdd");

        QList<QPair<QString,QString> > listeRelance = OBJ_bdd.getRelance ( dateRelance );

        debugger ("Relance manuelle", QString::number ( listeRelance.length () ) );

        ui->lineEdit_nbRelance->setText ( QString::number ( listeRelance.length () ) );

        afficherMessage("----------------------------", false);
        afficherMessage("Nombre de relance : " % QString::number ( listeRelance.length () ), false);

        //Objet relance
        Relance *relance = new Relance();

        connect (relance, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

        connect (relance, SIGNAL(lancerVisualisation(QString,QString)), this, SLOT(visualierPEC(QString,QString)));

        connect (relance, SIGNAL(traitementFiniRelance()), this, SLOT(finRelance()));

        connect (this, SIGNAL(on_pause_relance(bool)), relance, SLOT(pauseRelance(bool)));

        connect (relance, SIGNAL(on_incrementerVisualisation(int)),this, SLOT(incrementerCompteurRelance(int)));

        QtConcurrent::run(relance, &Relance::relancer, listeRelance, PATH_DOSSIER_CONFIGURATION);
    }
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de mettre a jour le compteur de relance en cours
  */
void MainWindow::incrementerCompteurRelance(int relanceEnCours){

    ui->lineEdit_relanceEnCours->setText ( QString::number ( relanceEnCours ) );
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite connaitres
  */
void MainWindow::on_actionStatistiques_retour_triggered(){

    QStringList header;
    header << "Jour" << "Occurence";

    Form_stats *stats = new Form_stats(header, "retour");
    connect (stats, SIGNAL(on_message(QString)), this, SLOT(popUp(QString)));
    stats->show ();
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher un message
  Si alerte TRUE alors on affiche un popUp warning
  */
void MainWindow::afficherMessage(QString message, bool alerte){

    ecrireLog ( NOM_FICHIER_LOG, message, true );
    ecrireIHM ( message );

    if( alerte )
        QMessageBox::warning (NULL,"Avertissement", message);
}



















//----------------------------------------------------------------------------------------------------------------------
/*
 * Lorsque l'utilisateur souhaite r�cup�re le fichier PDF image de la RPEC via un IDCHRONO de DPEC
 * */
void MainWindow::on_actionT_l_charger_RPEC_PDF_triggered(){

    //Demander la date de recherche
    QString idChronoSelec = QInputDialog::getText(this, "IDCHRONO", "ICHRONO : ", QLineEdit::Normal);

    if( !idChronoSelec.isEmpty () ){

        idChrono = idChronoSelec;

        afficherMessage( idChrono % " en cours..", false);

        telechargerRpec ( getRnm ( idChrono ) );
    }
}

















//----------------------------------------------------------------------------------------------------------------------
/*
 * Lorsque l'utilisateur lance manuellement la collecte de mail sur l'adresse webservice-4axes@4axes.fr
 * */
void MainWindow::on_actionPopper_triggered(){

    Emailing *PopMail = new Emailing();

    receptionMail = true;

    connect(PopMail, SIGNAL(on_message(QString,bool)), SLOT(afficherMessage(QString,bool)));

    connect(PopMail, SIGNAL(popMailFini()), this, SLOT(finPop()));

    connect(PopMail, SIGNAL(reconcilierPec(QString,QString)), this, SLOT(reconcilierMessage(QString,QString)));

    connect(this, SIGNAL(on_pause_traitementMail(bool)), PopMail, SLOT(setPause(bool)));

    connect(PopMail, SIGNAL(on_envoyerMail(QString)), this, SLOT(envoyerMail(QString)));

    connect(PopMail, SIGNAL(on_deplacerFichier(QString,QString,bool)), this, SLOT(deplacerFichier(QString,QString,bool)));

    connect(PopMail, SIGNAL(on_envoyerMailPj(QString,QString,QString)), this,
            SLOT(envoyerPieceJointe(QString,QString,QString,QString)));

    QtConcurrent::run(PopMail, &Emailing::recupererPieceJointe, QCoreApplication::applicationDirPath());
}









//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionTraiter_PDF_triggered(){

    //Ouverture de la bo�te de dialogue
    QString pathDossier = QFileDialog::getExistingDirectory (this, "Dossier", QCoreApplication::applicationDirPath ());

    if( !pathDossier.isEmpty() ){

        receptionMail = true;

        Emailing *PopMail = new Emailing();

        connect(PopMail, SIGNAL(on_message(QString,bool)), SLOT(afficherMessage(QString,bool)));

        connect(PopMail, SIGNAL(popMailFini()), this, SLOT(finPop()));

        connect(PopMail, SIGNAL(reconcilierPec(QString,QString)), this, SLOT(reconcilierMessage(QString,QString)));

        connect(this, SIGNAL(on_pause_traitementMail(bool)), PopMail, SLOT(setPause(bool)));

        connect(PopMail, SIGNAL(on_envoyerMail(QString)), this, SLOT(envoyerMail(QString)));

        connect(PopMail, SIGNAL(on_deplacerFichier(QString,QString,bool)), this, SLOT(deplacerFichier(QString,QString,bool)));

        connect(PopMail, SIGNAL(on_envoyerMailPj(QString,QString,QString)), this,
                SLOT(envoyerPieceJointe(QString,QString,QString,QString)));

        QtConcurrent::run(PopMail, &Emailing::traiterFichier, pathDossier);
    }
}
