#include "configuration.h"

#define NOM_FICHIER_CONFIGURATION           QString("config.ini")

Configuration::Configuration()
{
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le chemin d'acc�s du NAS
  */
QString Configuration::getPathNas(){

    QSettings settings( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_CONFIGURATION, QSettings::IniFormat);
    return( settings.value ("Path/NAS","Inconnu").toString () );
}






//----------------------------------------------------------------------------------------------------------------------
QString Configuration::getPathRelance(){

    QSettings settings( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_CONFIGURATION, QSettings::IniFormat);
    return( settings.value ("Path/Relance","Inconnu").toString () );
}






//----------------------------------------------------------------------------------------------------------------------
QString Configuration::getPathRenvoi(){

    QSettings settings( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_CONFIGURATION, QSettings::IniFormat);
    return( settings.value ("Path/Renvoi","Inconnu").toString () );
}











//----------------------------------------------------------------------------------------------------------------------
/*
    Retourne la liste des cl�s d'un groupe concern� [groupe]
    pour un fichier de configuration concern� [fichierINI]
  */
QStringList Configuration::getListeCle(QString fichierINI, QString groupe){

    QSettings settings( fichierINI, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    return( settings.allKeys () );
}










//----------------------------------------------------------------------------------------------------------------------
/*
    Retourne la valeur d'une cl� donn�e [key]
    pour un groupe concern� [groupe]
    dans un fichier pr�cis� [fichierINI]
  */
QString Configuration::getValue(QString fichierINI, QString groupe, QString key){

    QSettings settings( fichierINI, QSettings::IniFormat);
    settings.beginGroup (groupe);
    return( settings.value (key,"-1").toString () );
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de savoir si un groupe existe [groupe] existe dans un fichier de configuratuin [fichierINI]
  */
bool Configuration::siGroupeExiste(QString fichierINI, QString groupe){

    QSettings settings( fichierINI, QSettings::IniFormat);
    QStringList listeGroupe = settings.childGroups ();
    return( listeGroupe.count ( groupe ) );
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne VRAI si la cl� [cle]
  existe dans le groupe [groupe]
  dans le fichier de configuration [fichierINI]
  */
bool Configuration::siCleExiste(QString fichierINI, QString groupe, QString cle){

    QSettings settings( fichierINI, QSettings::IniFormat);
    settings.beginGroup (groupe);
    return( settings.contains ( cle ) );

}
