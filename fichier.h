#ifndef FICHIER_H
#define FICHIER_H

#include <QObject>
#include <QStringList>
#include <QString>
#include <QTextDocument>
#include <QPrinter>
#include <QStringBuilder>
#include <QDate>
#include <QtXmlPatterns/QXmlQuery>

#include "configuration.h"

#define PATH_DOSSIER_CONFIGURATION        QString(QCoreApplication::applicationDirPath () % "/Configuration")

class Fichier : public QObject
{
    Q_OBJECT

public:
    Fichier();

    void insererDonnees(QString fluxXmlDpec, QString patron, QStringList listParametre, QString pathDossierTemp, QString idChrono,
                        QString etatRetour, QString libelleRefus);

    void imprimerFichier(QString donnees, QString pathDossierTemp, QString idChrono);

    QString getInformationPatient(QString rnm, QString fluxXmlDpec, QString information);

signals:

    void on_message(QString message, bool alerte);

    void on_reconcilierFichier(QString pathFichier, QString idChrono);
};

#endif // FICHIER_H
