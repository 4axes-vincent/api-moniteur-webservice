/********************************************************************************
** Form generated from reading UI file 'fen_statistique.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FEN_STATISTIQUE_H
#define UI_FEN_STATISTIQUE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_Fen_statistique
{
public:
    QCustomPlot *widget_stats;
    QCalendarWidget *calendrier;
    QCheckBox *checkBox_journeeStats;
    QCheckBox *checkBox_periodeStats;
    QPushButton *btn_exporter;
    QLineEdit *lineEdit_dateFin;
    QLabel *label;
    QLineEdit *lineEdit_dateDebut;
    QRadioButton *radioButton_webService;
    QPushButton *btn_genererStats;
    QRadioButton *radioButton_4axes;
    QLabel *label_titre;
    QComboBox *comboBox_mutuelle;

    void setupUi(QWidget *Fen_statistique)
    {
        if (Fen_statistique->objectName().isEmpty())
            Fen_statistique->setObjectName(QStringLiteral("Fen_statistique"));
        Fen_statistique->resize(1062, 707);
        widget_stats = new QCustomPlot(Fen_statistique);
        widget_stats->setObjectName(QStringLiteral("widget_stats"));
        widget_stats->setGeometry(QRect(30, 240, 1011, 441));
        calendrier = new QCalendarWidget(Fen_statistique);
        calendrier->setObjectName(QStringLiteral("calendrier"));
        calendrier->setGeometry(QRect(30, 70, 256, 155));
        checkBox_journeeStats = new QCheckBox(Fen_statistique);
        checkBox_journeeStats->setObjectName(QStringLiteral("checkBox_journeeStats"));
        checkBox_journeeStats->setGeometry(QRect(90, 40, 70, 17));
        checkBox_periodeStats = new QCheckBox(Fen_statistique);
        checkBox_periodeStats->setObjectName(QStringLiteral("checkBox_periodeStats"));
        checkBox_periodeStats->setGeometry(QRect(190, 40, 70, 17));
        btn_exporter = new QPushButton(Fen_statistique);
        btn_exporter->setObjectName(QStringLiteral("btn_exporter"));
        btn_exporter->setGeometry(QRect(950, 210, 91, 23));
        lineEdit_dateFin = new QLineEdit(Fen_statistique);
        lineEdit_dateFin->setObjectName(QStringLiteral("lineEdit_dateFin"));
        lineEdit_dateFin->setGeometry(QRect(310, 140, 113, 20));
        label = new QLabel(Fen_statistique);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(360, 120, 21, 16));
        lineEdit_dateDebut = new QLineEdit(Fen_statistique);
        lineEdit_dateDebut->setObjectName(QStringLiteral("lineEdit_dateDebut"));
        lineEdit_dateDebut->setGeometry(QRect(310, 90, 113, 20));
        radioButton_webService = new QRadioButton(Fen_statistique);
        radioButton_webService->setObjectName(QStringLiteral("radioButton_webService"));
        radioButton_webService->setGeometry(QRect(470, 210, 81, 17));
        btn_genererStats = new QPushButton(Fen_statistique);
        btn_genererStats->setObjectName(QStringLiteral("btn_genererStats"));
        btn_genererStats->setGeometry(QRect(630, 210, 121, 23));
        radioButton_4axes = new QRadioButton(Fen_statistique);
        radioButton_4axes->setObjectName(QStringLiteral("radioButton_4axes"));
        radioButton_4axes->setGeometry(QRect(560, 210, 61, 17));
        label_titre = new QLabel(Fen_statistique);
        label_titre->setObjectName(QStringLiteral("label_titre"));
        label_titre->setGeometry(QRect(500, 10, 61, 16));
        comboBox_mutuelle = new QComboBox(Fen_statistique);
        comboBox_mutuelle->setObjectName(QStringLiteral("comboBox_mutuelle"));
        comboBox_mutuelle->setGeometry(QRect(310, 200, 131, 22));

        retranslateUi(Fen_statistique);

        QMetaObject::connectSlotsByName(Fen_statistique);
    } // setupUi

    void retranslateUi(QWidget *Fen_statistique)
    {
        Fen_statistique->setWindowTitle(QApplication::translate("Fen_statistique", "Form", 0));
        checkBox_journeeStats->setText(QApplication::translate("Fen_statistique", "Journ\303\251e", 0));
        checkBox_periodeStats->setText(QApplication::translate("Fen_statistique", "P\303\251riode", 0));
        btn_exporter->setText(QApplication::translate("Fen_statistique", "Exporter", 0));
        label->setText(QApplication::translate("Fen_statistique", "Au", 0));
        radioButton_webService->setText(QApplication::translate("Fen_statistique", "webService", 0));
        btn_genererStats->setText(QApplication::translate("Fen_statistique", "G\303\251n\303\251rer statistique", 0));
        radioButton_4axes->setText(QApplication::translate("Fen_statistique", "4axes", 0));
        label_titre->setText(QApplication::translate("Fen_statistique", "Statistiques", 0));
    } // retranslateUi

};

namespace Ui {
    class Fen_statistique: public Ui_Fen_statistique {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FEN_STATISTIQUE_H
