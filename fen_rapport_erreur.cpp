#include "fen_rapport_erreur.h"
#include "ui_fen_rapport_erreur.h"

Fen_rapport_erreur::Fen_rapport_erreur(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Fen_rapport_erreur)
{
    ui->setupUi(this);
    this->setWindowTitle ("Rapport erreur");
}

Fen_rapport_erreur::~Fen_rapport_erreur()
{
    delete ui;
}
