QStringList listeValeurRetour;

    //On envoi vers 4AXES.NET seulement les refus ou les accords
    if( etat == "accord" || etat == "refus" ){

        afficherMessage("Retour envoye a 4AXES.NET pour l'idChrono : " % idChronoDpec, false);
        afficherMessage("Etat : " % etat, false);

        if( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm(idChronoDpec) % "_RPEC.ini", "Action", "telechargerRpec" ) == "1" ){

            //ALMERYS
            if( OBJ_bdd.getValue ("idMessage", idChronoDpec ).isEmpty() ){

                afficherMessage("Numero de PEC operateur inexistant pour la DPEC " % idChronoDpec, false);

                genererPDF(listeValeurRetour, getRnm ( idChronoDpec ), etat, libelle );
            }
            else
                telechargerRpec( getRnm ( idChronoDpec ) );
        }
        else if( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm(idChronoDpec) % "_RPEC.ini", "Action", "genererRpec" ) == "1" ){

            //TODO : � poursuivre
//            listParametre = creeerParametre(rpecXML, getRnm ( idChronoDpec ) );

//            genererPDF( listParametre, getRnm ( idChronoDpec ), etat, libelle );
        }
        else if( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm(idChronoDpec) % "_RPEC.ini", "Action", "PopperRpec" ) == "1" ){

            //Pour VIEMADIS
            if( OBJ_bdd.getValue ("idMessage", idChronoDpec ).isEmpty() ){

                afficherMessage("Numero de PEC operateur inexistant pour la DPEC " % idChronoDpec, false);

                listeValeurRetour = creeerParametre(rpecXML, getRnm ( idChronoDpec ) );

                integrerRetour( idChronoDpec, listeValeurRetour, etat, libelle);
            }
            else{

                if( OBJ_bdd.getValue("etatTransmission", idChronoDpec).isEmpty() ){

                    Emailing *Popper = new Emailing();

                    connect(Popper, SIGNAL(on_message(QString,bool)), SLOT(afficherMessage(QString,bool)));

                    connect(Popper, SIGNAL(popMailFini()), this, SLOT(finPop()));

                    connect(Popper, SIGNAL(reconcilierPec(QString,QString)), this, SLOT(reconcilierMessage(QString,QString)));

                    connect(this, SIGNAL(on_pause_traitementMail(bool)), Popper, SLOT(setPause(bool)));

                    connect(Popper, SIGNAL(on_envoyerMail(QString)), this, SLOT(envoyerMail(QString)));

                    connect(Popper, SIGNAL(on_deplacerFichier(QString,QString,bool)), this, SLOT(deplacerFichier(QString,QString,bool)));

                    receptionMail = true;

                    QtConcurrent::run(Popper, &Emailing::recupererPieceJointe);
                }
                else
                    afficherMessage("L'IDCHRONO " % idChronoDpec % " a deja obtenu la reponse de prise en charge au format PDF", false);
            }
        }
        else if( config.getValue ( PATH_DOSSIER_CONFIGURATION % "/" % getRnm(idChronoDpec) % "_RPEC.ini", "Action", "integrerRpec" ) == "1" ){

            listeValeurRetour = creeerParametre(rpecXML, getRnm ( idChronoDpec ) );

            integrerRetour( idChronoDpec, listeValeurRetour, etat, libelle);
        }
	else{

		afficherMessage ("Aucune action configuree pour reconcilier la demande initiale avec le retour", false);

		if( siRelance || renvoiRpec)
			emit on_pause_relance ( false );
		else
			emit on_stop_iteration ( false );
	}