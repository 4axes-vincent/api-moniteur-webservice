#ifndef FORM_STATS_H
#define FORM_STATS_H

#include <QWidget>
#include <QTableWidgetItem>
#include <QStringList>
#include <QDate>
#include <QTextStream>
#include <QMessageBox>

#define PATH_EXPORT         QString( QCoreApplication::applicationDirPath () % "/Export")


#include "database.h"

namespace Ui {
class Form_stats;
}

class Form_stats : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_stats(QStringList listeNomColonne, QString typeStat, QWidget *parent = 0);
    ~Form_stats();

    void afficherResultat( QList<QPair<QString, QString> > resultat );
    void listerMutuelle();
    
private slots:

    void on_checkBox_journee_clicked(bool checked);

    void on_checkBox_periode_clicked(bool checked);

    void on_calendrier_clicked(const QDate &date);

    void on_btn_genererStats_clicked();

    void envoyerMessage(QString message, bool alerte);

    void on_btn_exporter_clicked();

    void on_btn_exporterFichierrRefus_clicked();

signals:

    void on_message(QString message, bool alerte);

private:
    Ui::Form_stats *ui;

    int clikID;
    QString contexte;
};

#endif // FORM_STATS_H
