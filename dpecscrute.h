#ifndef DPECSCRUTE_H
#define DPECSCRUTE_H

#include <QObject>
#include <QThread>
#include <QString>
#include <QStringBuilder>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QSettings>
#include <QMutex>
#include <QDateTime>
#include <QFile>
#include <QCoreApplication>


class Dpecscrute : public QThread
{
    Q_OBJECT

public:
    Dpecscrute();

    void scruter(QString path, QString pathRenvoi);

private slots:

    void stoperIterateur(bool flag);

signals:

    void on_fichierEnCours(QString nomFichier);

    void on_aucunFichier();

    void on_message(QString msg, bool alerte);

    void on_renvoyerFichier(QString pathFichier, QString contexte);

private:

    bool stop;
};

#endif // DPECSCRUTE_H
