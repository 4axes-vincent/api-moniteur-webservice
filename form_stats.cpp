#include "form_stats.h"
#include "ui_form_stats.h"

Form_stats::Form_stats(QStringList listeNomColonne, QString typeStat, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_stats)
{
    ui->setupUi(this);
    this->setWindowTitle ("Statistiques");

    ui->tableWidget->setColumnCount ( listeNomColonne.length () );
    ui->tableWidget->setHorizontalHeaderLabels ( listeNomColonne );
    ui->tableWidget->horizontalHeader ()->resizeSections(QHeaderView::Stretch);

    clikID = 0;

    ui->lineEdit_dateDebut->setAlignment ( Qt::AlignCenter);
    ui->lineEdit_dateFin->setAlignment ( Qt::AlignCenter);

    listerMutuelle();

    contexte = typeStat;
}

Form_stats::~Form_stats()
{
    delete ui;
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher le r�sultat pass� en param�tre sur le tableau

  */
void Form_stats::afficherResultat(QList<QPair<QString, QString> > resultat){

    if( !resultat.isEmpty () ){

        ui->comboBox_listeRefus->clear ();

        ui->tableWidget->setRowCount (0);

        for(int i = 0 ; i < resultat.length () ; i++){

            ui->tableWidget->insertRow (i);

            QTableWidgetItem *itemOccurence = new QTableWidgetItem( resultat.at (i).first );
            itemOccurence->setTextAlignment (Qt::AlignCenter);

            QTableWidgetItem *itemMotif = new QTableWidgetItem( resultat.at (i).second );
            itemMotif->setTextAlignment (Qt::AlignCenter);

            ui->tableWidget->setItem (i,0, itemOccurence);
            ui->tableWidget->setItem (i,1, itemMotif);

            ui->comboBox_listeRefus->addItem ( itemMotif->text () );
        }
    }
    else
        emit on_message ("Aucun r�sultat", true);

}
















//----------------------------------------------------------------------------------------------------------------------
void Form_stats::listerMutuelle(){

    Database *bdd = new Database();
    connect (bdd, SIGNAL(on_erreur(QString)), this, SLOT(envoyerMessage(QString)));
    connect (bdd, SIGNAL(on_message(QString)), this, SLOT(envoyerMessage(QString)));

    QStringList listeMutuelle = bdd->getListeRnm ();

    if( !listeMutuelle.isEmpty () )
        ui->comboBox_mutuelles->addItems ( listeMutuelle );

    else
        emit on_message ("Aucune mutuelle en base de donn�es", true);
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur la checkBox "Journ�e"
  */
void Form_stats::on_checkBox_journee_clicked(bool checked){

    ui->checkBox_periode->setChecked ( false );
    ui->checkBox_periode->setEnabled ( !checked );
    ui->lineEdit_dateFin->clear ();
    ui->lineEdit_dateFin->setEnabled ( !checked );
    ui->label_au->setEnabled ( !checked );

    ui->lineEdit_dateDebut->setText ( QDate::currentDate ().toString ("dd/MM/yyyy") );
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur cloque sur la checkBox "P�riode"
  */
void Form_stats::on_checkBox_periode_clicked(bool checked){

    ui->checkBox_journee->setChecked ( false );
    ui->checkBox_journee->setEnabled ( !checked );
    ui->lineEdit_dateDebut->clear ();
    ui->lineEdit_dateFin->clear ();
    ui->lineEdit_dateFin->setEnabled ( true );
    ui->label_au->setEnabled ( true );

}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur une date du calendrier
  */
void Form_stats::on_calendrier_clicked(const QDate &date){

    if( ui->checkBox_journee->isChecked () || ui->checkBox_periode->isChecked () ){

        if( ui->checkBox_journee->isChecked () )
            ui->lineEdit_dateDebut->setText ( date.toString ("dd/MM/yyyy") );

        else if( ui->checkBox_periode->isChecked () ){

            if( clikID == 0 ){

                ui->lineEdit_dateDebut->setText ( date.toString ("dd/MM/yyyy") );
                clikID = 1;
            }
            else{

                ui->lineEdit_dateFin->setText ( date.toString ("dd/MM/yyyy") );
                clikID = 0;
            }
        }
    }
    else
        emit on_message ("Veuillez s�lectionner le crit�re journ�e ou p�riode", true);
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lancer la requ�te selon les crit�res (journ�e ou date)
  */
void Form_stats::on_btn_genererStats_clicked(){

    //On v�rifie que l'utilisateur a au moins pris une option
    if( ui->checkBox_journee->isChecked () || ui->checkBox_periode->isChecked () ){

        //Objet base de donn�es
        Database *bdd = new Database();
        connect (bdd, SIGNAL(on_erreur(QString)), this, SLOT(envoyerMessage(QString)));
        connect (bdd, SIGNAL(on_message(QString)), this, SLOT(envoyerMessage(QString)));

        //Le r�sultat de la requ�te
        QList<QPair<QString, QString> > listePaireResultat;

        //Conversion de la date au format yyyyMMdd
        QString dateDebut = QDate::fromString ( ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd");

        //Si crit�re journ�e
        if( ui->checkBox_journee->isChecked () ){

            //V�rification date
            if( !ui->lineEdit_dateDebut->text ().isEmpty () ){

                listePaireResultat = bdd->getStatsRefus ( dateDebut,"-1", ui->comboBox_mutuelles->currentText () );
                afficherResultat( listePaireResultat );
            }
            else
                emit on_message ("Veuillez renseigner une date pour votre recherche par journ�e", true);
        }

        //Si crit�re journ�e
        else if( ui->checkBox_periode->isChecked () ){

            //V�rification des dates
            if( !ui->lineEdit_dateDebut->text ().isEmpty () && !ui->lineEdit_dateFin->text ().isEmpty () ){

                QString dateFin = QDate::fromString ( ui->lineEdit_dateFin->text (), "dd/MM/yyyy").toString ("yyyyMMdd");

                if( contexte == "refus")
                    listePaireResultat = bdd->getStatsRefus ( dateDebut, dateFin, ui->comboBox_mutuelles->currentText () );

                else if( contexte == "retour")
                    listePaireResultat = bdd->getStatsRetour ( dateDebut, dateFin, ui->comboBox_mutuelles->currentText () );

                afficherResultat( listePaireResultat );
            }
            else
                emit on_message ("Veuillez renseigner une date de d�but et/ou une date de fin pour votre rechercher par", true);
        }
        delete bdd;
    }
    else
        emit on_message ("Veuillez s�lectionner une recherche par journ�e ou par p�riode", true);

}















//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi le message r�ceptionn� de la classe appel� vers la classe appelante
  */
void Form_stats::envoyerMessage(QString message, bool alerte){

    emit on_message (message, alerte);
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Exporter" pour exporter ces donn�es (format CSV)
  */
void Form_stats::on_btn_exporter_clicked(){

    QFile fichierExport( PATH_EXPORT % "/Export_refus_" % QDateTime::currentDateTime ().toString ("yyyyMMdd_hh_mm_ss") % ".csv");
    if( ! fichierExport.open ( QIODevice::Text | QIODevice::ReadWrite | QIODevice::Append ) )
        emit on_message ("Erreur ouverture fichier export - Erreur : " % fichierExport.errorString (), true );
    else{

        QTextStream fluxExport(&fichierExport);

        fluxExport << "Occurence" << ";" << "Libell� refus" << endl;

        for(int i = 0 ; i < ui->tableWidget->rowCount () ; i++){

            fluxExport << ui->tableWidget->item (i,0)->text () << ";" << ui->tableWidget->item (i,1)->text () << endl;
        }

        QMessageBox::information (NULL,"Export statistique", "Statistiques export�es et envoy�es pour contr�le");

        fichierExport.close ();
    }
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet � l'utilisateur d'exporter les fichiers refus d'une journ�e
  pr�cise pour les analyser
  */
void Form_stats::on_btn_exporterFichierrRefus_clicked(){

    if( ui->checkBox_journee->isChecked () ){

        //Objet base de donn�es
        Database *bdd = new Database();
        connect (bdd, SIGNAL(on_erreur(QString)), this, SLOT(envoyerMessage(QString)));
        connect (bdd, SIGNAL(on_message(QString)), this, SLOT(envoyerMessage(QString)));

        //Conversion de la date au format yyyyMMdd
        QString dateDebut = QDate::fromString ( ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd");

        QStringList listeResultat = bdd->getValue ("idChrono", "dateDpec" , dateDebut, "motif", ui->comboBox_listeRefus->currentText () );

        QFileInfo fichierInfo;
        QFile fichier;
        QString dateRpec;

        for(int i = 0 ; i < listeResultat.length () ; i++){

            dateRpec = bdd->getValue ("dateRpec", listeResultat.at (i) );

            QDirIterator iterateur( QCoreApplication::applicationDirPath ()
                                    % "/History/" % ui->comboBox_mutuelles->currentText () % "/" % dateRpec, QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot );

            while( iterateur.hasNext () ){

                fichierInfo = iterateur.next ();

                if( fichierInfo.fileName ().contains ( listeResultat.at (i) ) && (fichierInfo.fileName ().contains ("RPEC")
                                                                                  || fichierInfo.fileName ().contains ("DPEC") ) ){

                    if( !fichier.copy ( fichierInfo.absoluteFilePath (), PATH_EXPORT % "/" % fichierInfo.fileName () ) )
                        QMessageBox::warning (NULL,"Copie impossible", "Impossible de copier le fichier " % fichierInfo.absoluteFilePath ()
                                              % " - Erreur : " % fichier.errorString () );



                }
            }
        }
        QMessageBox::information (NULL,"Export", "Exportation termin�e");
    }
    else
        QMessageBox::warning (NULL,"Crit�re","Vous pouvez seulement exporter les refus d'une journ�e !");

}
