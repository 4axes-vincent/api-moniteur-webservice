/********************************************************************************
** Form generated from reading UI file 'fen_rapport_erreur.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FEN_RAPPORT_ERREUR_H
#define UI_FEN_RAPPORT_ERREUR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Fen_rapport_erreur
{
public:
    QTableWidget *tableWidget;

    void setupUi(QWidget *Fen_rapport_erreur)
    {
        if (Fen_rapport_erreur->objectName().isEmpty())
            Fen_rapport_erreur->setObjectName(QStringLiteral("Fen_rapport_erreur"));
        Fen_rapport_erreur->resize(946, 716);
        tableWidget = new QTableWidget(Fen_rapport_erreur);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(20, 220, 911, 481));

        retranslateUi(Fen_rapport_erreur);

        QMetaObject::connectSlotsByName(Fen_rapport_erreur);
    } // setupUi

    void retranslateUi(QWidget *Fen_rapport_erreur)
    {
        Fen_rapport_erreur->setWindowTitle(QApplication::translate("Fen_rapport_erreur", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class Fen_rapport_erreur: public Ui_Fen_rapport_erreur {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FEN_RAPPORT_ERREUR_H
