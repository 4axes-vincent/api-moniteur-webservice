#ifndef FEN_STATISTIQUE_H
#define FEN_STATISTIQUE_H

#include <QWidget>
#include <QDate>
#include <iostream>

#include "database.h"

#define PATH_EXPORT         QString( QCoreApplication::applicationDirPath () % "/Export")

namespace Ui {
class Fen_statistique;
}

class Fen_statistique : public QWidget
{
    Q_OBJECT
    
public:
    explicit Fen_statistique(QWidget *parent = 0);
    ~Fen_statistique();

    void afficherGraphPoint (QVector<double> vecteurY);
    void listerMutuelle();
    
private slots:
    void on_calendrier_clicked(const QDate &date);

    void on_checkBox_journeeStats_clicked(bool checked);

    void on_checkBox_periodeStats_clicked(bool checked);

    void on_btn_genererStats_clicked();

    void on_btn_exporter_clicked();

    void on_radioButton_webService_clicked();

    void on_radioButton_4axes_clicked();

    void envoyerMessage(QString message, bool alerte);

signals:
    void on_message(QString message, bool alerte);

private:
    Ui::Fen_statistique *ui;

    int clikID;
    QString contexte;
};

#endif // FEN_STATISTIQUE_H
