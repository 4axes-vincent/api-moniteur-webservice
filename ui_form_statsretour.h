/********************************************************************************
** Form generated from reading UI file 'form_statsretour.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_STATSRETOUR_H
#define UI_FORM_STATSRETOUR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_statsRetour
{
public:
    QCalendarWidget *calendarWidget;
    QLineEdit *lineEdit_dateDebut;
    QLineEdit *lineEdit_dateFin;
    QLabel *label_titre;
    QLabel *label_au;
    QCheckBox *checkBox_journee;
    QCheckBox *checkBox_periode;
    QFrame *line;

    void setupUi(QWidget *Form_statsRetour)
    {
        if (Form_statsRetour->objectName().isEmpty())
            Form_statsRetour->setObjectName(QStringLiteral("Form_statsRetour"));
        Form_statsRetour->resize(463, 536);
        calendarWidget = new QCalendarWidget(Form_statsRetour);
        calendarWidget->setObjectName(QStringLiteral("calendarWidget"));
        calendarWidget->setGeometry(QRect(100, 50, 256, 155));
        lineEdit_dateDebut = new QLineEdit(Form_statsRetour);
        lineEdit_dateDebut->setObjectName(QStringLiteral("lineEdit_dateDebut"));
        lineEdit_dateDebut->setGeometry(QRect(110, 250, 91, 20));
        lineEdit_dateFin = new QLineEdit(Form_statsRetour);
        lineEdit_dateFin->setObjectName(QStringLiteral("lineEdit_dateFin"));
        lineEdit_dateFin->setGeometry(QRect(260, 250, 91, 20));
        label_titre = new QLabel(Form_statsRetour);
        label_titre->setObjectName(QStringLiteral("label_titre"));
        label_titre->setGeometry(QRect(170, 20, 121, 21));
        label_au = new QLabel(Form_statsRetour);
        label_au->setObjectName(QStringLiteral("label_au"));
        label_au->setGeometry(QRect(220, 250, 21, 20));
        checkBox_journee = new QCheckBox(Form_statsRetour);
        checkBox_journee->setObjectName(QStringLiteral("checkBox_journee"));
        checkBox_journee->setGeometry(QRect(140, 220, 71, 17));
        checkBox_periode = new QCheckBox(Form_statsRetour);
        checkBox_periode->setObjectName(QStringLiteral("checkBox_periode"));
        checkBox_periode->setGeometry(QRect(270, 220, 70, 17));
        line = new QFrame(Form_statsRetour);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(30, 290, 401, 16));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        retranslateUi(Form_statsRetour);

        QMetaObject::connectSlotsByName(Form_statsRetour);
    } // setupUi

    void retranslateUi(QWidget *Form_statsRetour)
    {
        Form_statsRetour->setWindowTitle(QApplication::translate("Form_statsRetour", "Form", 0));
        label_titre->setText(QApplication::translate("Form_statsRetour", "Statistiques des retours", 0));
        label_au->setText(QApplication::translate("Form_statsRetour", "Au", 0));
        checkBox_journee->setText(QApplication::translate("Form_statsRetour", "Journ\303\251e", 0));
        checkBox_periode->setText(QApplication::translate("Form_statsRetour", "P\303\251riode", 0));
    } // retranslateUi

};

namespace Ui {
    class Form_statsRetour: public Ui_Form_statsRetour {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_STATSRETOUR_H
