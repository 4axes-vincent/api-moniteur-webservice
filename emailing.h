#ifndef EMAILING_H
#define EMAILING_H

#include <QObject>
#include <QThread>
#include <QStringList>
#include <QString>
#include <QStringBuilder>
#include <QProcess>
#include <QDir>
#include <QFileInfoList>
#include <QXmlStreamReader>
#include <QTemporaryFile>

#include <QtXmlPatterns/QXmlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <QHttpMultiPart>
#include <QHttpPart>

#include "database.h"


#define HTTP_DOMAINE_4AXES              QString("domaine")
#define HTTP_DOMAINE_4AXES_VALEUR       QString("4axes")

#define PATH_POPPER                     QString(QCoreApplication::applicationDirPath() % "/Popper")
#define PATH_RETOUR_PDF                 QString(QCoreApplication::applicationDirPath () % "/RetourPDF")
#define PATH_RETOUR_PDF_INC              QString(QCoreApplication::applicationDirPath () % "/RetourPDF/Inconnu")
#define PATH_DOSSIER_HISTORY            QString(QCoreApplication::applicationDirPath () % "/History")

class Emailing : public QThread
{
    Q_OBJECT

public:
    Emailing();

    void recupererPieceJointe(QString pathMoniteur);

    void lancerReconciliation(QString path);

    void parserReponse(QString reponse, QString pathFichier, int temps);

    void traiterFichier(QString pathDossierAnalyse);

    int getNombreFichier (QString dossierReception);

    QString getRnm(QString idChrono);

signals:

    void on_message(QString message, bool alerte);

    void on_envoyerMail(QString message);

    void on_envoyerMailPj(QString pathFichier, QString msg, QString objet, QString destinataire);

    void on_deplacerFichier(QString pathFichier, QString pathDossierDestination, bool couperFichier);

    void reconcilierPec(QString idChrono, QString pathFichier);

    void popMailFini();

private slots:

    void envoyerMessage(QString message, bool alerte);

    void setPause(bool flag);

private:

    bool stop;

    QString idChronoDpec;
    QString pathFichierPdf;

    QTime *tempsEnvoi;
};

#endif // EMAILING_H
