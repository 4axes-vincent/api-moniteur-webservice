#include "emailing.h"

Emailing::Emailing()
{
}





//----------------------------------------------------------------------------------------------------------------------
/*
  Lance le popper pour r�cup�rer seulement les pi�ce jointes des fichier EML
  */
void Emailing::recupererPieceJointe(QString pathMoniteur){

    emit on_message("Mise en attente volonaire, reception de l'email sur webservice-4axes@4axes.fr", false);

    //Si une instance est deja en train de r�cup�rer des mails
    while( QFile::exists( pathMoniteur % "/popEnCours") ){

        emit on_message("En attente de la connexion pour recuperer les e-mails", false);
        QThread::currentThread()->msleep(3000);
    }

    QFile fichierPopEncours(pathMoniteur % "/popEnCours");
    fichierPopEncours.open(QIODevice::Text);

    QThread::msleep(3000);

    QStringList argumentCollect;
    argumentCollect << "-u";

    //On lance la collecte
    QProcess *process = new QProcess();

    process->start ( PATH_POPPER % "/FaxSmartDownloader.exe", argumentCollect );
    process->waitForFinished (-1);

    //Gestion de code de retour pour la collecte des e-mails
    if( process->exitCode () == -1)
        emit on_message ("Echec de la connexion au serveur de mail..( " % QString::number( process->exitCode() ), false);

    else if( process->exitCode ()  == 0 ){

        emit on_message ("Connexion serveur e-mail OK - Recuperation des e-mails", false);

        QThread::currentThread()->msleep(1000);

        int nbEmailCollecter = getNombreFichier( PATH_POPPER % "/RPEC" );

        if( nbEmailCollecter > 0 ){

            emit on_message ("Reception de " % QString::number ( nbEmailCollecter ) % " e-mail(s)", false);

            QStringList argumentExtraction;
            argumentExtraction << "-ud" << PATH_POPPER % "/RPEC";

            //On appelle le programme FaxSmartDownloader
            process->start ( PATH_POPPER % "/FaxSmartDownloader.exe", argumentExtraction);
            process->waitForFinished (-1);

            //Gestion de code de retour pour l'extraction des pi�ces jointe
            if( process->exitCode () == -1 )
                emit on_message ("Code de sortie : " % QString::number ( process->exitCode () ), false );

            else{

                emit on_message ( "Code de sortie : " %  QString::number ( process->exitCode () ), false);

                traiterFichier( PATH_POPPER % "/RPEC" );
            }
        }
        else{

            emit on_message("Aucun e-mail recuperer", false);

            emit popMailFini();
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Parcours le dossier contenant les PDF, RPEC de VIAMEDIS, pour les r�concilier sur 4axes.net
  */
void Emailing::lancerReconciliation(QString path){

    QString numDossier;
    QFileInfo fichierInfo;

    QDir dossier(path);
    QStringList listeFichier = dossier.entryList(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

    stop = false;

    Database *bdd = new Database();

    connect (bdd, SIGNAL(on_erreur(QString)), this, SLOT(envoyerMessage(QString)));

    QRegExp reg("\\D");

    //Parcourt la liste de PDF r�cup�rer via le popper
    for(int i = 0 ; i < listeFichier.length() ; i++){

        fichierInfo.setFile( path % "/" % listeFichier.at(i) );

        numDossier = fichierInfo.fileName().mid(0, fichierInfo.fileName().lastIndexOf("_") );
        numDossier = numDossier.replace(reg, "");

        idChronoDpec = bdd->getValue ( "idChrono", "idMessage", numDossier );

        stop = true;

        if( !idChronoDpec.isEmpty () ){

            emit on_message ("Numero PEC operateur : " % numDossier, false);

            emit reconcilierPec(fichierInfo.absoluteFilePath (), idChronoDpec );
        }
        else{

            emit on_message ("---------------------------------", false);

//            emit on_message ("Aucun IDCHRONO ayant le numero PEC operateur : " % numDossier, false);

            emit on_envoyerMail("Aucun IDCHRONO ayant le numero PEC operateur " % numDossier
                                % ", le fichier PDF est transmis au backOffice");

            emit on_envoyerMailPj( path % "/" % listeFichier.at(i), "","Nouveau fax 0326773731", "poperocr@4axes.fr");

            emit on_deplacerFichier(fichierInfo.absoluteFilePath(), PATH_RETOUR_PDF_INC % "/" % fichierInfo.fileName(), true);

            stop = false;

            emit on_message ("---------------------------------", false);
        }

        while( stop ){
            QThread::currentThread ()->msleep (1000);
        }
    }

    delete bdd;

    emit popMailFini();
}






















































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de d�placer des fichiers d'une certaine extension d'un dossier d'origine � un dossier de destination
  */
void Emailing::traiterFichier(QString pathDossierAnalyse){

    QDir dossier( pathDossierAnalyse );
    QStringList filtre;
    filtre << "*.pdf";

    QStringList listeFichiers = dossier.entryList(filtre, QDir::Files | QDir::NoDotAndDotDot );

    QFileInfo fichierInfo;

    //Parcourir le dossier de fichier(s)
    for(int i = 0 ; i < listeFichiers.length() ; i++){

        fichierInfo.setFile( pathDossierAnalyse % "/" % listeFichiers.at(i) );

        emit on_deplacerFichier(fichierInfo.absoluteFilePath(), PATH_RETOUR_PDF % "/" % fichierInfo.fileName(), true);

        QThread::currentThread()->msleep(250);
    }

    lancerReconciliation( PATH_RETOUR_PDF );
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le nombre de fichier e-mail t�l�charg�
  */
int Emailing::getNombreFichier (QString dossierReception){

    QDir dossier( dossierReception );
    QStringList filtre;
    filtre << "*.pdf";

    //On compte le nombre de fichier dans le dossier cible
    QFileInfoList entryList = dossier.entryInfoList ( filtre, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    return( entryList.count () );
}














//----------------------------------------------------------------------------------------------------------------------
/*
 * Retourne le RNM contenant dans l'IDCHRONO
 * */
QString Emailing::getRnm(QString idChrono){

    return( idChrono.split("_").at(1) );
}








//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de r�ceptionner un message provenant d'une classe appel� vers la classe appelante
  */
void Emailing::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte );
}








//----------------------------------------------------------------------------------------------------------------------
void Emailing::setPause(bool flag){

    stop = flag;
}

