#-------------------------------------------------
#
# Project created by QtCreator 2016-01-13T15:47:57
#
#-------------------------------------------------

QT       += core gui network xml sql xmlpatterns widgets printsupport

TARGET = MoniteurWebService
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    configuration.cpp \
    fen_statistique.cpp \
    qcustomplot.cpp \
    fen_rapport_erreur.cpp \
    dpecscrute.cpp \
    form_stats.cpp \
    relance.cpp \
    form_statsretour.cpp \
    database.cpp \
    message.cpp \
    emailing.cpp \
    fichier.cpp

HEADERS  += mainwindow.h \
    configuration.h \
    fen_statistique.h \
    qcustomplot.h \
    fen_rapport_erreur.h \
    dpecscrute.h \
    form_stats.h \
    relance.h \
    form_statsretour.h \
    database.h \
    message.h \
    emailing.h \
    fichier.h

FORMS    += mainwindow.ui \
    fen_statistique.ui \
    fen_rapport_erreur.ui \
    form_stats.ui \
    form_statsretour.ui


RC_FILE = ressource.rc
