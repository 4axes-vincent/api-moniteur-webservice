#include "dpecscrute.h"

Dpecscrute::Dpecscrute()
{
}




//----------------------------------------------------------------------------------------------------------------------
/*
  Scrute le r�pertoire PATH
  */
void Dpecscrute::scruter(QString path, QString pathRenvoi){

    stop = false;
    QMutex mutex;

    QFileInfo fichierInfo;
    QDir dossier(path);
//    QDir dosssierRenvoi( pathRenvoi );

    while( !QFile::exists ( QCoreApplication::applicationDirPath () % "/stopWS" ) ){

        QStringList listeFichier = dossier.entryList (QDir::Files | QDir::NoDotAndDotDot);

        if( !listeFichier.isEmpty() ){

            for(int i = 0 ; i < listeFichier.length () ; i++){

                fichierInfo.setFile ( path % "/" % listeFichier.at (i) );

                if( fichierInfo.baseName().contains("creation") ){

                    mutex.lock ();

                    stop = true;

                    //Indique le fichier en cours
                    emit on_fichierEnCours( fichierInfo.absoluteFilePath () );

                    mutex.unlock ();
                }

                while(stop){
                    QThread::msleep (1000);
                }
            }

            QThread::currentThread ()->msleep (1000);

            emit on_message("-*-*-*-*-*-*-*-*-*-*-", false);
            emit on_message("Gestion des annulations de PEC", false);

            for(int i = 0 ; i < listeFichier.length () ; i++){

                fichierInfo.setFile ( path % "/" % listeFichier.at (i) );

                if( fichierInfo.baseName().contains("annulation") ){

                    mutex.lock ();

                    stop = true;

                    //Indique le fichier en cours
                    emit on_fichierEnCours( fichierInfo.absoluteFilePath () );

                    mutex.unlock ();
                }

                while(stop){
                    QThread::msleep (1000);
                }
            }
        }


//        QStringList listeRenvoi = dosssierRenvoi.entryList(QDir::Files | QDir::NoDotAndDotDot);

//        if( listeRenvoi.length() > 0 ){

//            emit on_message("*-*-*-*-*-*-*-*-*-*-*-*", false);
//            emit on_message("Nombre de DPEC a renvoyer : " % QString::number( listeRenvoi.length() ), false);

//            for(int j = 0 ; j < listeRenvoi.length() ; j++){

//                mutex.lock ();

//                stop = true;

//                fichierInfo.setFile ( pathRenvoi % "/" % listeRenvoi.at (j) );

//                emit on_renvoyerFichier( fichierInfo.absoluteFilePath(), "recreer" );

//                mutex.unlock();

//                while(stop){
//                    QThread::msleep (1000);
//                }
//            }
//        }
        QThread::currentThread ()->msleep (5000);
    }



    emit on_message ("Arr�t du webService", false);

    QThread::currentThread ()->msleep (100);

    QThread::currentThread ()->quit ();
}







//----------------------------------------------------------------------------------------------------------------------
void Dpecscrute::stoperIterateur(bool flag){

    stop = flag;
}







