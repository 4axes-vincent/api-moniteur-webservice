#include "fichier.h"

Fichier::Fichier()
{

}


//----------------------------------------------------------------------------------------------------------------------
/*
 * Permet d'ins�rer les donn�es dans le fichier patron
 * */
void Fichier::insererDonnees(QString fluxXmlDpec, QString patron, QStringList listParametre, QString pathDossierTemp, QString idChrono,
                             QString etatRetour, QString libelleRefus){

    QString finess = idChrono.split("_").at(0);
    QString rnm = idChrono.split("_").at(1);

    QString nom = getInformationPatient( rnm, fluxXmlDpec, "nom" );
    QString prenom = getInformationPatient( rnm, fluxXmlDpec, "prenom" );
    QString dateNaissance = getInformationPatient(rnm, fluxXmlDpec, "dateNaissance");
    QString numeroInsee = getInformationPatient( rnm, fluxXmlDpec, "numInsee" );

    patron.replace("VAR_NUM_FINESS", finess );
    patron.replace("VAR_IDCHRONO", idChrono);

    patron.replace("VAR_NOM_PATIENT", nom);
    patron.replace("VAR_PRENOM_PATIENT", prenom);
    patron.replace("VAR_DATE_NAISS", dateNaissance);
    patron.replace("VAR_NUMERO_INSEE", numeroInsee);

    if( etatRetour == "accord" ){

        emit on_message("Insertion des donnees accord en cours de developpement", false);
    }
    else if( etatRetour == "refus" ){

        emit on_message("Libelle du refus : " % libelleRefus, false);

        patron.replace("VAR_REFUS", libelleRefus );
    }
    else
        emit on_message("Etat du retour inconnu", false);

    imprimerFichier( patron, pathDossierTemp, idChrono);
}










//----------------------------------------------------------------------------------------------------------------------
/*
 * Permet d'imprimer le fichier PDF apr�s insertion des donn�es
 * */
void Fichier::imprimerFichier(QString donnees, QString pathDossierTemp, QString idChrono){

    QTextDocument document;
    document.setHtml( donnees );

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName( pathDossierTemp % "/" % idChrono % ".pdf" );

    document.print(&printer);

    emit on_message("Generation fichier PDF termine", false);

    emit on_reconcilierFichier(pathDossierTemp % "/" % idChrono % ".pdf", idChrono);
}






//----------------------------------------------------------------------------------------------------------------------
/*
 * Permet de r�cup�rer des informations dans le flux de DPEC via des requ�tes XPATH configur�e dans le fichier de config
 * RNM_DPEC.ini
 * */
QString Fichier::getInformationPatient(QString rnm, QString fluxXmlDpec, QString information){

    QXmlQuery queryXml;

    if( queryXml.setFocus ( fluxXmlDpec ) ){

        Configuration *config = new Configuration();

        QString valeur;

        QString requeteXPath = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Information_Dpec", information );

        //Requ�te XPath correspondant � la colonne
        queryXml.setQuery ( requeteXPath );
        queryXml.evaluateTo (&valeur);

        //Nettoyage de la zone
        valeur.replace ("\n","");

        return ( valeur );
    }
    else
        emit on_message ("Impossible de charger le flux XML dans le contexte de generation de PDF", true);
}
