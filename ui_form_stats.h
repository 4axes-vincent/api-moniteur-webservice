/********************************************************************************
** Form generated from reading UI file 'form_stats.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_STATS_H
#define UI_FORM_STATS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_stats
{
public:
    QTableWidget *tableWidget;
    QLabel *label_titre;
    QCalendarWidget *calendrier;
    QLineEdit *lineEdit_dateDebut;
    QLineEdit *lineEdit_dateFin;
    QLabel *label_au;
    QPushButton *btn_genererStats;
    QPushButton *btn_exporter;
    QCheckBox *checkBox_journee;
    QCheckBox *checkBox_periode;
    QCheckBox *checkBox_touteMutuelle;
    QComboBox *comboBox_mutuelles;
    QFrame *line;
    QLabel *label_refus;
    QComboBox *comboBox_listeRefus;
    QPushButton *btn_exporterFichierrRefus;

    void setupUi(QWidget *Form_stats)
    {
        if (Form_stats->objectName().isEmpty())
            Form_stats->setObjectName(QStringLiteral("Form_stats"));
        Form_stats->resize(1005, 708);
        tableWidget = new QTableWidget(Form_stats);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(280, 100, 711, 591));
        label_titre = new QLabel(Form_stats);
        label_titre->setObjectName(QStringLiteral("label_titre"));
        label_titre->setGeometry(QRect(470, 30, 56, 21));
        calendrier = new QCalendarWidget(Form_stats);
        calendrier->setObjectName(QStringLiteral("calendrier"));
        calendrier->setGeometry(QRect(10, 100, 256, 155));
        lineEdit_dateDebut = new QLineEdit(Form_stats);
        lineEdit_dateDebut->setObjectName(QStringLiteral("lineEdit_dateDebut"));
        lineEdit_dateDebut->setGeometry(QRect(20, 300, 101, 20));
        lineEdit_dateFin = new QLineEdit(Form_stats);
        lineEdit_dateFin->setObjectName(QStringLiteral("lineEdit_dateFin"));
        lineEdit_dateFin->setGeometry(QRect(160, 300, 101, 20));
        label_au = new QLabel(Form_stats);
        label_au->setObjectName(QStringLiteral("label_au"));
        label_au->setGeometry(QRect(130, 300, 21, 21));
        btn_genererStats = new QPushButton(Form_stats);
        btn_genererStats->setObjectName(QStringLiteral("btn_genererStats"));
        btn_genererStats->setGeometry(QRect(140, 340, 61, 23));
        btn_exporter = new QPushButton(Form_stats);
        btn_exporter->setObjectName(QStringLiteral("btn_exporter"));
        btn_exporter->setGeometry(QRect(210, 340, 61, 23));
        checkBox_journee = new QCheckBox(Form_stats);
        checkBox_journee->setObjectName(QStringLiteral("checkBox_journee"));
        checkBox_journee->setGeometry(QRect(70, 270, 70, 17));
        checkBox_periode = new QCheckBox(Form_stats);
        checkBox_periode->setObjectName(QStringLiteral("checkBox_periode"));
        checkBox_periode->setGeometry(QRect(150, 270, 70, 17));
        checkBox_touteMutuelle = new QCheckBox(Form_stats);
        checkBox_touteMutuelle->setObjectName(QStringLiteral("checkBox_touteMutuelle"));
        checkBox_touteMutuelle->setGeometry(QRect(20, 380, 101, 17));
        comboBox_mutuelles = new QComboBox(Form_stats);
        comboBox_mutuelles->setObjectName(QStringLiteral("comboBox_mutuelles"));
        comboBox_mutuelles->setGeometry(QRect(20, 340, 111, 22));
        line = new QFrame(Form_stats);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(10, 430, 251, 16));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        label_refus = new QLabel(Form_stats);
        label_refus->setObjectName(QStringLiteral("label_refus"));
        label_refus->setGeometry(QRect(10, 460, 51, 16));
        comboBox_listeRefus = new QComboBox(Form_stats);
        comboBox_listeRefus->setObjectName(QStringLiteral("comboBox_listeRefus"));
        comboBox_listeRefus->setGeometry(QRect(50, 460, 191, 22));
        btn_exporterFichierrRefus = new QPushButton(Form_stats);
        btn_exporterFichierrRefus->setObjectName(QStringLiteral("btn_exporterFichierrRefus"));
        btn_exporterFichierrRefus->setGeometry(QRect(160, 490, 81, 23));

        retranslateUi(Form_stats);

        QMetaObject::connectSlotsByName(Form_stats);
    } // setupUi

    void retranslateUi(QWidget *Form_stats)
    {
        Form_stats->setWindowTitle(QApplication::translate("Form_stats", "Form", 0));
        label_titre->setText(QApplication::translate("Form_stats", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Statistiques</span></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; mar"
                        "gin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p></body></html>", 0));
        label_au->setText(QApplication::translate("Form_stats", "Au", 0));
        btn_genererStats->setText(QApplication::translate("Form_stats", "G\303\251n\303\251rer", 0));
        btn_exporter->setText(QApplication::translate("Form_stats", "Exporter", 0));
        checkBox_journee->setText(QApplication::translate("Form_stats", "Journ\303\251e", 0));
        checkBox_periode->setText(QApplication::translate("Form_stats", "P\303\251riode", 0));
        checkBox_touteMutuelle->setText(QApplication::translate("Form_stats", "Toutes mutuelles", 0));
        label_refus->setText(QApplication::translate("Form_stats", "Refus :", 0));
        btn_exporterFichierrRefus->setText(QApplication::translate("Form_stats", "Exporter", 0));
    } // retranslateUi

};

namespace Ui {
    class Form_stats: public Ui_Form_stats {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_STATS_H
