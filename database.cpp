#include "Database.h"

#define UTILISATEUR_BDD             QString("4axes")
#define MOT_DE_PASSE_BDD            QString("fgbmo78")
#define DATABASE                    QString("QSQLITE")
#define HOSTNAME                    QString("localhost")
#define DATABASE_NAME               QString(QCoreApplication::applicationDirPath () % "/FRONTAL")

Database::Database(){


}

void Database::creerTable(){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        //Creation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_frontal  ( "
                " idChrono VARCHAR(255),"
                " idEntite VARCHAR(255),"
                " idMessage VARCHAR(255),"
                " dateDpec VARCHAR(10),"
                " dateRpec VARCHAR(10),"
                " dateRelance VARCHAR(10),"
                " nombreRelance INTEGER,"
                " action VARCHAR(100),"
                " annulation VARCHAR(3),"
                " etatRpec  VARCHAR(10),"
                " motif VARCHAR(255),"
                " tempsRetour INTEGER(255),"
                " etatTransmission VARCHAR(255),"
                " tempsTransmission INTEGER(255),"
                " tempsEditionPec INTEGER(255),"
                " editionRpec VARCHAR(255),"
                " PRIMARY KEY(idChrono) );");
    }

    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}



//----------------------------------------------------------------------------------------------------------------------
bool Database::ajouterDonnee(QString idChrono, QString contexteRequete){

    if( contexteRequete.contains ("visualiser") || contexteRequete.contains ("annuler") )
        return( true );

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
        return( false );
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "INSERT INTO tbl_frontal VALUES('" % idChrono % "',"
                " '""',"
                " '""','"
                % QDate::currentDate ().toString ("yyyyMMdd") % "',"
                " '""',"
                " '""',"
                " 0,"
                " '""',"
                " '""',"
                " 'En cours',"
                " '""',"
                " 0,"
                " '""',"
                " 0,0,'""');";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
            return( false );
        }
        else
            return( true );
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}






//----------------------------------------------------------------------------------------------------------------------
bool Database::incrementerRelance(QString idChrono){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
    else{

        QSqlQuery sqlQuery;
        QString requete;

        requete = "SELECT nombreRelance FROM tbl_frontal WHERE idChrono='" % idChrono % "';";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
            return( false );
        }
        else{

            QString data;

            //Parcours des resultats
            while( sqlQuery.next () ){

                data = sqlQuery.value (0).toString ();
            }
            sqlQuery.clear ();

            requete = "UPDATE tbl_FRONTAL SET dateRelance='" % QDate::currentDate ().toString ("yyyyMMdd")
                    % "', nombreRelance='" % QString::number (data.toInt () + 1) % "' WHERE idChrono='" % idChrono % "';";

            //Si mauvaise execution
            if( !sqlQuery.exec ( requete ) ){

                emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                                % sqlQuery.lastError ().text (), true);
                return ( false );
            }
            else
                return( true );

        }
    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de mettre a jour des donnees dans la base de donnees
  */
bool Database::majDonnee(QString idChrono, QString colonne, QString valeur){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() ){

        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
        return( false );
    }
    else{

        if( !valeur.isEmpty () ){

            QString requete;
            QSqlQuery sqlQuery;

            valeur.replace ("'"," ");

            requete = "UPDATE tbl_FRONTAL SET " % colonne % " = '" % valeur % "' WHERE idChrono = '" % idChrono % "';";

            if( !sqlQuery.exec ( requete ) ){

                emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                                % sqlQuery.lastError ().text (), true);
                return( false );
            }
            else
                return( true );

        }
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}













//----------------------------------------------------------------------------------------------------------------------
QList< QPair<QString, QString> > Database::getRelance(QString dateRef){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
    else{

        QString requete;
        QSqlQuery sqlQuery;

        requete = "SELECT idChrono, dateDpec FROM tbl_frontal WHERE dateDpec < '" % dateRef
                % "' AND etatRpec = 'retourEnAttente' AND dateRelance < '" % dateRef % "';";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

        else{

            QList<QPair<QString, QString> > listePaire;
            QPair<QString, QString> paire;

            //Parcours des resultats
            while( sqlQuery.next () ){

                paire.first = sqlQuery.value (0).toString ();
                paire.second = sqlQuery.value (1).toString ();

                listePaire.push_back ( paire );

                paire.first.clear ();
                paire.second.clear ();
            }
            return( listePaire );
        }
    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne la valeur de la colonne [colonne] pour la ligne contenant l'idChrono [idChrono]
  */
QString Database::getValue(QString colonne, QString idChrono){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
    else{

        QString requete;
        QSqlQuery sqlQuery;

        requete = "SELECT " % colonne % " FROM tbl_frontal where idChrono = '" % idChrono % "';";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

        else{

            QString data;

            //Parcours des resultats
            while( sqlQuery.next () ){

                data = sqlQuery.value ( 0 ).toString ();
            }
            return( data );
        }
    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();

}












//----------------------------------------------------------------------------------------------------------------------
/*
    Retourne la valeur d'une colonne selon une clause where d'une condition
  */
QString Database::getValue(QString colonne, QString condition, QString valeur){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
    else{

        QString requete;
        QSqlQuery sqlQuery;

        requete = "SELECT " % colonne % " FROM tbl_frontal where " % condition % " = '" % valeur % "';";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

        else{

            QString data;

            //Parcours des resultats
            while( sqlQuery.next () ){

                data = sqlQuery.value ( 0 ).toString ();
            }
            return( data );
        }
    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();


}



















//----------------------------------------------------------------------------------------------------------------------
/*
    Retourne la valeur d'une colonne selon une clause where d'une condition
  */
QStringList Database::getValue(QString colonne, QString condition_1, QString valeur_1, QString condition_2, QString valeur_2){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
    else{

        QString requete;
        QSqlQuery sqlQuery;

        requete = "SELECT " % colonne % " FROM tbl_frontal where " % condition_1 % " = '" % valeur_1 % "' AND " % condition_2 % " = '" % valeur_2 % "' ;";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

        else{

            QStringList data;

            //Parcours des resultats
            while( sqlQuery.next () ){

                data.push_back (sqlQuery.value ( 0 ).toString ());
            }
            return( data );
        }
    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne TRUE si la colonne existe dans la table
  */
bool Database::siColonneExiste(QString colonne){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
    else{

        QSqlQuery sqlQuery;
        QString requete = "PRAGMA table_info (tbl_frontal);";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

        else{

            //Parcours des resultats
            while( sqlQuery.next () ){

                if( colonne == sqlQuery.value ( 1 ).toString () )
                    return(true);
            }
            return(false);
        }
    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de verifier si un IDCHRONO [idChrono] est present dans la base de donnees
  */
bool Database::verifierDonnee(QString idChrono){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    //Test d'ouverture
    if( ! db.open() ){

        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );
        return(false);
    }
    else{

        QSqlQuery sqlQuery;
        sqlQuery.clear ();

        QString requete = "SELECT count(*) FROM tbl_frontal where idChrono = '" % idChrono % "';";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

        else{

            //Parcours des resultats
            while( sqlQuery.next () ){

                if( sqlQuery.value (0).toString ().toInt () > 0 )
                    return(true);
            }
            return(false);
        }
    }
}













//----------------------------------------------------------------------------------------------------------------------
/*
  Enregistre le nom du fichier et le motif de l'erreur rencontree
  */
void Database::enregistrerErreur(QString nomFichier, QString motifErreur){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        //Creation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_frontal_erreur ( nomFichier VARCHAR(255), date VARCHAR(10), heure VARCHAR(10),"
                " motifErreur VARCHAR(255) ); ");

        QSqlQuery sqlQuery;
        QString requete = "INSERT INTO tbl_frontal_erreur VALUES('" % nomFichier % "',"
                " '" % QDate::currentDate().toString ("yyyyMMdd") % "', '"
                % QDateTime::currentDateTime ().toString ("hh:mm:ss")
                % "', '" % motifErreur % "');";

        //Si mauvaise execution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}














//----------------------------------------------------------------------------------------------------------------------
QVector<double> Database::getStats(QString dateDebut, QString dateFin, QString contexte, QString rnm){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete;

        if( dateFin == "-1" )
            requete = "SELECT " % contexte % " FROM tbl_frontal WHERE dateRpec == " % dateDebut
                    % " AND idEntite = '" % rnm % "';";
        else
            requete = "SELECT " % contexte % " FROM tbl_frontal WHERE dateRpec >= " % dateDebut
                    % " AND dateRpec <= "
                    % dateFin % " AND idEntite = '" % rnm % "';";

        if( sqlQuery.exec ( requete) ){

            QVector<double> vecteur;

            //Parcours des resultats
            while( sqlQuery.next () ){

                vecteur.push_back ( sqlQuery.value (0).toString ().toDouble () / 1000 );
            }
            return( vecteur );
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);

    }
    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();

}












//----------------------------------------------------------------------------------------------------------------------
QList<QPair<QString, QString> > Database::getStatsRefus(QString dateDebut, QString dateFin, QString rnm){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete;

        if( dateFin == "-1" )
            requete = "SELECT count(*) as nbRefus, motif from tbl_frontal WHERE idEntite = '"
                    % rnm % "' AND etatRpec='refus' AND dateDpec = '" % dateDebut
                    % "' GROUP BY motif ORDER BY nbRefus DESC;";
        else
            requete = "SELECT count(*) as nbRefus, motif from tbl_frontal WHERE idEntite = '" % rnm
                    % "' AND etatRpec='refus' AND dateDpec >= '" % dateDebut
                    % "' AND dateDpec <= '" % dateFin % "'GROUP BY motif ORDER BY nbRefus DESC;";

        if( sqlQuery.exec ( requete) ){

           QList<QPair<QString, QString> > listePaire;
           QPair<QString, QString> paire;

            //Parcours des resultats
            while( sqlQuery.next () ){

                paire.first = sqlQuery.value (0).toString ();
                paire.second = sqlQuery.value (1).toString ();

                listePaire.push_back ( paire );

                paire.first.clear ();
                paire.second.clear ();
            }
            return( listePaire );
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
}














//----------------------------------------------------------------------------------------------------------------------
QList<QPair<QString, QString> > Database::getStatsRetour(QString dateDebut, QString dateFin, QString rnm){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete;

        QStringList liste;
        liste << "J" << "J+1" << "J+3" << "J+4";

        QStringList listeRequete;

        requete = "SELECT count(*) FROM tbl_frontal WHERE dateDpec >= '" % dateDebut % "' AND dateDpec <= '" % dateFin % "'"
                " AND dateDpec == dateRpec AND idEntite = '" % rnm % "';";

//        listeRequete.push_back ( requete );

//        requete = "SELECT count(*) FROM tbl_frontal WHERE dateDpec >= '" % dateDebut % "' AND dateDpec <= '" % dateFin % "'"
//                " AND dateDpec == dateRpec AND idEntite = '" % rnm % "';";

//        listeRequete.push_back ( requete );

        emit on_message ( requete, true );

        if( sqlQuery.exec ( requete) ){

           QList<QPair<QString, QString> > listePaire;
           QPair<QString, QString> paire;

           paire.first = "J";

            //Parcours des resultats
            while( sqlQuery.next () ){

                paire.second = sqlQuery.value (0).toString ();

                listePaire.push_back ( paire );
            }

            emit on_message ( paire.first % " - " % paire.second, true );

            return( listePaire );
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
}













//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne une liste contenant les RNM de toutes les mutuelles passant par le frontal
  */
QStringList Database::getListeRnm(){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QStringList listeRnm;

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT idEntite FROM tbl_frontal GROUP BY idEntite;";

        if( sqlQuery.exec ( requete) ){

            //Parcours des resultats
            while( sqlQuery.next () ){

                listeRnm.push_back (sqlQuery.value (0).toString ());
            }
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
    return( listeRnm );
}













//----------------------------------------------------------------------------------------------------------------------
bool Database::supprimerEnregistrement(QString colonne, QString valeur){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    bool executionRequete = false;

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "DELETE FROM tbl_frontal WHERE " % colonne % " = '" % valeur % "';";

        if( sqlQuery.exec ( requete) )
            executionRequete = true;

        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
    return( executionRequete );
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de compter le nombre d'enregistrement dans la table [table] de la colonne [colonne] suivant une valeur [valeur]
  */
QString Database::compterEnregistrement(QString table, QString colonne_1, QString valeur_1, QString colonne_2,
                                        QString valeur_2){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QString resultat = "-1";

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT COUNT(*) FROM " % table % " WHERE " % colonne_1 % " = '" % valeur_1 % "'"
                " AND " % colonne_2 % " = '" % valeur_2 % "';";

        if( sqlQuery.exec ( requete) ){

            //Parcours des resultats
            while( sqlQuery.next () ){

                resultat = sqlQuery.value (0).toString ();
            }
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
    return( resultat );
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de compter le nombre d'enregistrement dans la table [table] de la colonne [colonne] suivant une valeur [valeur]
  */
QStringList Database::getListeidChronoErreur(QString date){


    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QStringList listeResultat;

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT nomFichier FROM tbl_frontal_erreur WHERE date = '" % date % "'";

        if( sqlQuery.exec ( requete) ){

            //Parcours des resultats
            while( sqlQuery.next () ){

                listeResultat.push_back (sqlQuery.value (0).toString ());
            }
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
    return( listeResultat );
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la liste des idMessages ayant eu un trop grand nombre de visualisation sans reponse
  */
QStringList Database::getDossierSansReponse(int visualisationLimite){

    //Importation des parametres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QStringList listeResultat;

    if( ! db.open() )
        emit on_message("Impossible se connecter a la base de donnees - Veuillez verifier vos parametres de connexion "
                       % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT idMessage FROM tbl_frontal WHERE nombreRelance >= " % QString::number ( visualisationLimite )
                % " AND etatRpec = 'retourEnAttente' ";

        if( sqlQuery.exec ( requete) ){

            //Parcours des resultats
            while( sqlQuery.next () ){

                listeResultat.push_back (sqlQuery.value (0).toString ());
            }
        }
        else
            emit on_message ("Impossible d'executer la requ�te [ " %  requete % " ] | ERREUR : "
                            % sqlQuery.lastError ().text (), true);
    }
    return( listeResultat );
}


