#include "message.h"



Message::Message()
{
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Parse le message retour du webService
  */
void Message::parserRetourWs(QString idChronoDPEC, QString messageRetour, QString rnm, QString contexte){

    QMutex verrou;
    verrou.lock ();

    QThread::currentThread ()->msleep (100);

    QXmlQuery queryXml;

    if( queryXml.setFocus ( messageRetour ) ){

        //Objet base de donn�es
        Database *bdd = new Database();
        connect (bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

        //Objet configuration
        Configuration *config = new Configuration();

        if( contexte == "reponsePriseEnCharge" ){

            bdd->majDonnee( idChronoDPEC, "dateRpec", QDate::currentDate().toString("yyyyMMdd") );

            //On r�cup�re la liste des requ�tes � effectuer
            QStringList listeRequete = config->getListeCle ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Requete" );

            QString colonne;
            QString etatRetour;
            QString libelleRefus;
            QString requeteXPath;
            QString valeur;

            for(int i = 0 ; i < listeRequete.length () ; i++){

                //Nom de la colonne
                colonne = listeRequete.at (i);

                requeteXPath = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Requete", colonne );

                //Requ�te XPath correspondant � la colonne
                queryXml.setQuery ( requeteXPath );
                queryXml.evaluateTo (&valeur);

                //Nettoyage de la zone
                valeur.replace ("\n","");

                //Si le r�sulat de la requ�te doit �tre traduit
                if( config->siCleExiste ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Traduction", valeur ) ){

                    valeur = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Traduction", valeur );

                    bdd->majDonnee ( idChronoDPEC, colonne, valeur );
                }
                else
                    bdd->majDonnee ( idChronoDPEC, colonne, valeur );

                QThread::currentThread ()->msleep (100);
            }

            etatRetour = bdd->getValue ("etatRpec", "idChrono", idChronoDPEC);
            libelleRefus = bdd->getValue ("motif", "idChrono", idChronoDPEC);

            if( etatRetour == "accord" || etatRetour == "refus" ){

                if( etatRetour == "refus" && config->siCleExiste(PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini",
                                                                 "Rejet_technique", libelleRefus) ){

                    //Si le libell� est consid�r� comme un rejet technique
                    if( config->getValue( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Rejet_technique", libelleRefus ) == "1" ){

                        emit on_message("Rejet technique - la demande de prise en charge sera renvoyee plus tard", false);

                        //Si le rejet technique n'a pas de num�ro de PEC op�rateur alors on met un statut sp�cial
                        //Et on d�place le fichier dans le dossier RENVOI
                        if( bdd->getValue("idMessage", "idChrono", idChronoDPEC).isEmpty() ){

                            QString action;
                            QString actionEnCours = bdd->getValue( "action", "idChrono", idChronoDPEC );

                            if( actionEnCours == "creer" )
                                action = "creation";

                            else if( actionEnCours == "visualiser" )
                                action = "_RELANCE";

                            else if( actionEnCours == "annuler" )
                                action = "annulation";

                            bdd->majDonnee ( idChronoDPEC, "etatRpec", "aRenvoyer" );

                            emit on_deplacerFichier( PATH_DOSSIER_HISTORY % "/" % rnm % "/"
                                                     % bdd->getValue("dateDpec", "idChrono", idChronoDPEC) % "/" % idChronoDPEC % "_DPEC.xml",
                                                     config->getValue( PATH_FICHIER_CONFIG, "Path", "Renvoi") % "/" % idChronoDPEC % action % ".xml", false);
                        }
                        else
                            bdd->majDonnee ( idChronoDPEC, "etatRpec", "retourEnAttente" );


                        QThread::currentThread()->msleep(200);

                        emit on_dpecSuivante();
                    }
                    else
                        emit on_envoyerVers4axes( idChronoDPEC, messageRetour, etatRetour, libelleRefus );
                }
                else
                     emit on_envoyerVers4axes( idChronoDPEC, messageRetour, etatRetour, libelleRefus );
            }
            else{

                emit on_message("DPEC en attente de valorisation", false);

                emit on_dpecSuivante();
            }
        }
        else if( contexte == "editionRpec"){

            QString requete = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini",
                                                 "Telechargement", "fichier" );
            QString valeur;

            queryXml.setQuery ( requete );
            queryXml.evaluateTo (&valeur);

            valeur.replace ("\n","");

            //Si il n'y pas de contenu � r�cup�rer -> erreur
            if( valeur.isEmpty () ){

                requete = config->getValue ( PATH_DOSSIER_CONFIGURATION % "/" % rnm % "_RPEC.ini", "Telechargement"
                                             , "messageErreur" );

                queryXml.setQuery ( requete );
                queryXml.evaluateTo (&valeur);

                bdd->majDonnee ( idChronoDPEC, "editionRpec", "NOK : " % valeur);

                emit on_message ("Incident lors de la recuperation du fichier PDF : " % valeur, false);

                emit on_dpecSuivante();
            }
            else{

                bdd->majDonnee ( idChronoDPEC, "editionRpec", "OK");

                emit on_decoderPec(valeur, idChronoDPEC);
            }
        }

        delete bdd;

        delete config;
    }
    else
        emit on_message ("Erreur focus dans le contexte : " % contexte, true);

    verrou.unlock ();

    QThread::currentThread ()->quit ();
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi un message � la classe appelante
  */
void Message::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte );
}


























//----------------------------------------------------------------------------------------------------------------------
void Message::parserListeReponse(QString pathDossier){

    QDirIterator iterateur( pathDossier, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    QFileInfo fichierInfoRPEC;

    QString idChrono;

    pauseIteration = false;

    //Tantqu'il y a des fichiers dans le dossier
    while( iterateur.hasNext () ){

        while(pauseIteration){
            QThread::currentThread ()->msleep (1000);
        }

        //On r�cup�re le nom du fichier
        fichierInfoRPEC = iterateur.next ();

        //Traitement pour �liminer les suffixes
        if( fichierInfoRPEC.baseName ().contains ("_RPEC_RELANCE") )
            idChrono = fichierInfoRPEC.baseName ().replace ("_RPEC_RELANCE","");

        else if( fichierInfoRPEC.baseName ().contains ("_RPEC") )
            idChrono = fichierInfoRPEC.baseName ().replace ("_RPEC","");

        emit on_message("---------------------------", false);
        emit on_message ( "Traitement de IDCHRONO : " % idChrono, false);

        //Ouverture du fichier XML pour r�cup�rer le flux
        QFile fichierRPEC( fichierInfoRPEC.absoluteFilePath () );
        if( ! fichierRPEC.open ( QIODevice::Text | QIODevice::ReadWrite ) )
            emit on_message ("Erreur ouverture fichier PREC - Erreur : " % fichierRPEC.errorString (), true );
        else{

            QTextStream fluxRPEC(&fichierRPEC);

            QString xml = fluxRPEC.readAll ();

            fichierRPEC.close ();

            pauseIteration = true;

            //On parse la r�ponse
            parserRetourWs( idChrono, xml, idChrono.split ("_").at (1), "reponsePriseEnCharge" );
        }
    }

    emit on_FinRenvoi();
}




























//----------------------------------------------------------------------------------------------------------------------
void Message::setPauseIteration(bool flag){

    pauseIteration = flag;
}














