#ifndef FEN_RAPPORT_ERREUR_H
#define FEN_RAPPORT_ERREUR_H

#include <QWidget>

namespace Ui {
class Fen_rapport_erreur;
}

class Fen_rapport_erreur : public QWidget
{
    Q_OBJECT
    
public:
    explicit Fen_rapport_erreur(QWidget *parent = 0);
    ~Fen_rapport_erreur();
    
private:
    Ui::Fen_rapport_erreur *ui;
};

#endif // FEN_RAPPORT_ERREUR_H
