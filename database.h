#ifndef DIAL_BDD_H
#define DIAL_BDD_H

#include <QCoreApplication>
#include <QThread>
#include <QObject>
#include <QString>
#include <QDate>
#include <QMessageBox>
#include <QStringBuilder>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QPair>
#include <QList>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlResult>



class Database : public QThread
{
    Q_OBJECT

public:
    Database();

    void creerTable();

    bool ajouterDonnee(QString idChrono, QString contexteRequete);

    bool incrementerRelance(QString idChrono);

    bool majDonnee(QString idChrono, QString colonne, QString valeur);

    QList<QPair<QString, QString> > getRelance(QString dateRef);

    QString getValue(QString colonne, QString idChrono);

    QString getValue(QString colonne, QString condition, QString valeur);

    QStringList getValue(QString colonne, QString condition_1, QString valeur_1, QString condition_2, QString valeur_2);

    bool siColonneExiste(QString colonne);

    bool verifierDonnee(QString idChrono);

    void enregistrerErreur(QString nomFichier, QString motifErreur);

    QVector<double> getStats(QString dateDebut, QString dateFin, QString contexte, QString rnm);

    QList<QPair<QString,QString> > getStatsRefus(QString dateDebut, QString dateFin, QString rnm);

    QList<QPair<QString,QString> > getStatsRetour(QString dateDebut, QString dateFin, QString rnm);

    QStringList getListeRnm();

    bool supprimerEnregistrement(QString colonne, QString valeur);

    QString compterEnregistrement(QString table, QString colonne_1, QString valeur_1, QString colonne_2, QString valeur_2);

    QStringList getListeidChronoErreur(QString date);

    QStringList getDossierSansReponse( int visualisationLimite );

signals:
    void on_message(QString message, bool alerte);

private:
    QSqlDatabase db;

};

#endif // DIAL_BDD_H
