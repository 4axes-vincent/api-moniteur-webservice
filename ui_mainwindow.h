/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionMise_jour;
    QAction *actionA_Propos;
    QAction *actionStatistiques;
    QAction *actionQuitter;
    QAction *actionRapport_d_erreur;
    QAction *actionEnvoyer_RPEC;
    QAction *actionConsulter_RPEC;
    QAction *actionStatistique_refus;
    QAction *actionTest_envoi_mail;
    QAction *actionRelancer_depuis_une_date;
    QAction *actionStatistiques_retour;
    QAction *actionT_l_charger_RPEC_PDF;
    QAction *actionPopper;
    QAction *actionTraiter_PDF;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_adrSite;
    QLabel *label_site;
    QLabel *label_action;
    QLineEdit *lineEdit_pathNAS;
    QLabel *label_nbRelance;
    QLineEdit *lineEdit_action;
    QLabel *label_pathNas;
    QLabel *label_relanceEnCours;
    QLineEdit *lineEdit_nbRelance;
    QListWidget *zoneTexte;
    QLineEdit *lineEdit_relanceEnCours;
    QFrame *line_3;
    QLabel *label;
    QMenuBar *menuBar;
    QMenu *menuFichier;
    QMenu *menuEdition;
    QMenu *menuAide;
    QMenu *menuRelance;
    QMenu *menuMail;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(770, 755);
        actionMise_jour = new QAction(MainWindow);
        actionMise_jour->setObjectName(QStringLiteral("actionMise_jour"));
        actionA_Propos = new QAction(MainWindow);
        actionA_Propos->setObjectName(QStringLiteral("actionA_Propos"));
        actionStatistiques = new QAction(MainWindow);
        actionStatistiques->setObjectName(QStringLiteral("actionStatistiques"));
        actionQuitter = new QAction(MainWindow);
        actionQuitter->setObjectName(QStringLiteral("actionQuitter"));
        actionRapport_d_erreur = new QAction(MainWindow);
        actionRapport_d_erreur->setObjectName(QStringLiteral("actionRapport_d_erreur"));
        actionEnvoyer_RPEC = new QAction(MainWindow);
        actionEnvoyer_RPEC->setObjectName(QStringLiteral("actionEnvoyer_RPEC"));
        actionConsulter_RPEC = new QAction(MainWindow);
        actionConsulter_RPEC->setObjectName(QStringLiteral("actionConsulter_RPEC"));
        actionStatistique_refus = new QAction(MainWindow);
        actionStatistique_refus->setObjectName(QStringLiteral("actionStatistique_refus"));
        actionTest_envoi_mail = new QAction(MainWindow);
        actionTest_envoi_mail->setObjectName(QStringLiteral("actionTest_envoi_mail"));
        actionRelancer_depuis_une_date = new QAction(MainWindow);
        actionRelancer_depuis_une_date->setObjectName(QStringLiteral("actionRelancer_depuis_une_date"));
        actionStatistiques_retour = new QAction(MainWindow);
        actionStatistiques_retour->setObjectName(QStringLiteral("actionStatistiques_retour"));
        actionT_l_charger_RPEC_PDF = new QAction(MainWindow);
        actionT_l_charger_RPEC_PDF->setObjectName(QStringLiteral("actionT_l_charger_RPEC_PDF"));
        actionPopper = new QAction(MainWindow);
        actionPopper->setObjectName(QStringLiteral("actionPopper"));
        actionTraiter_PDF = new QAction(MainWindow);
        actionTraiter_PDF->setObjectName(QStringLiteral("actionTraiter_PDF"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lineEdit_adrSite = new QLineEdit(centralWidget);
        lineEdit_adrSite->setObjectName(QStringLiteral("lineEdit_adrSite"));

        gridLayout->addWidget(lineEdit_adrSite, 2, 1, 1, 4);

        label_site = new QLabel(centralWidget);
        label_site->setObjectName(QStringLiteral("label_site"));

        gridLayout->addWidget(label_site, 2, 0, 1, 1);

        label_action = new QLabel(centralWidget);
        label_action->setObjectName(QStringLiteral("label_action"));

        gridLayout->addWidget(label_action, 3, 0, 1, 1);

        lineEdit_pathNAS = new QLineEdit(centralWidget);
        lineEdit_pathNAS->setObjectName(QStringLiteral("lineEdit_pathNAS"));

        gridLayout->addWidget(lineEdit_pathNAS, 4, 1, 1, 4);

        label_nbRelance = new QLabel(centralWidget);
        label_nbRelance->setObjectName(QStringLiteral("label_nbRelance"));

        gridLayout->addWidget(label_nbRelance, 5, 0, 1, 2);

        lineEdit_action = new QLineEdit(centralWidget);
        lineEdit_action->setObjectName(QStringLiteral("lineEdit_action"));

        gridLayout->addWidget(lineEdit_action, 3, 1, 1, 4);

        label_pathNas = new QLabel(centralWidget);
        label_pathNas->setObjectName(QStringLiteral("label_pathNas"));

        gridLayout->addWidget(label_pathNas, 4, 0, 1, 1);

        label_relanceEnCours = new QLabel(centralWidget);
        label_relanceEnCours->setObjectName(QStringLiteral("label_relanceEnCours"));

        gridLayout->addWidget(label_relanceEnCours, 5, 3, 1, 1);

        lineEdit_nbRelance = new QLineEdit(centralWidget);
        lineEdit_nbRelance->setObjectName(QStringLiteral("lineEdit_nbRelance"));

        gridLayout->addWidget(lineEdit_nbRelance, 5, 2, 1, 1);

        zoneTexte = new QListWidget(centralWidget);
        zoneTexte->setObjectName(QStringLiteral("zoneTexte"));

        gridLayout->addWidget(zoneTexte, 6, 0, 1, 7);

        lineEdit_relanceEnCours = new QLineEdit(centralWidget);
        lineEdit_relanceEnCours->setObjectName(QStringLiteral("lineEdit_relanceEnCours"));

        gridLayout->addWidget(lineEdit_relanceEnCours, 5, 4, 1, 1);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_3, 1, 0, 1, 7);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 3, 1, 3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 770, 21));
        menuFichier = new QMenu(menuBar);
        menuFichier->setObjectName(QStringLiteral("menuFichier"));
        menuEdition = new QMenu(menuBar);
        menuEdition->setObjectName(QStringLiteral("menuEdition"));
        menuAide = new QMenu(menuBar);
        menuAide->setObjectName(QStringLiteral("menuAide"));
        menuRelance = new QMenu(menuBar);
        menuRelance->setObjectName(QStringLiteral("menuRelance"));
        menuMail = new QMenu(menuBar);
        menuMail->setObjectName(QStringLiteral("menuMail"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFichier->menuAction());
        menuBar->addAction(menuEdition->menuAction());
        menuBar->addAction(menuRelance->menuAction());
        menuBar->addAction(menuMail->menuAction());
        menuBar->addAction(menuAide->menuAction());
        menuFichier->addAction(actionQuitter);
        menuEdition->addAction(actionStatistiques);
        menuEdition->addAction(actionStatistique_refus);
        menuEdition->addAction(actionStatistiques_retour);
        menuEdition->addAction(actionRapport_d_erreur);
        menuEdition->addAction(actionEnvoyer_RPEC);
        menuEdition->addAction(actionConsulter_RPEC);
        menuEdition->addAction(actionT_l_charger_RPEC_PDF);
        menuAide->addAction(actionMise_jour);
        menuAide->addAction(actionA_Propos);
        menuAide->addAction(actionTest_envoi_mail);
        menuRelance->addAction(actionRelancer_depuis_une_date);
        menuMail->addAction(actionPopper);
        menuMail->addAction(actionTraiter_PDF);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionMise_jour->setText(QApplication::translate("MainWindow", "Mise \303\240 jour", 0));
        actionA_Propos->setText(QApplication::translate("MainWindow", "A Propos", 0));
        actionStatistiques->setText(QApplication::translate("MainWindow", "Statistiques", 0));
        actionQuitter->setText(QApplication::translate("MainWindow", "Quitter", 0));
        actionRapport_d_erreur->setText(QApplication::translate("MainWindow", "Rapport d'erreur", 0));
        actionEnvoyer_RPEC->setText(QApplication::translate("MainWindow", "Envoyer RPEC", 0));
        actionConsulter_RPEC->setText(QApplication::translate("MainWindow", "Consulter RPEC", 0));
        actionStatistique_refus->setText(QApplication::translate("MainWindow", "Statistique refus", 0));
        actionTest_envoi_mail->setText(QApplication::translate("MainWindow", "Test envoi mail", 0));
        actionRelancer_depuis_une_date->setText(QApplication::translate("MainWindow", "Relancer depuis une date", 0));
        actionStatistiques_retour->setText(QApplication::translate("MainWindow", "Statistique retour", 0));
        actionT_l_charger_RPEC_PDF->setText(QApplication::translate("MainWindow", "T\303\251l\303\251charger RPEC PDF", 0));
        actionPopper->setText(QApplication::translate("MainWindow", "Popper", 0));
        actionTraiter_PDF->setText(QApplication::translate("MainWindow", "Traiter PDF", 0));
        label_site->setText(QApplication::translate("MainWindow", "Site :", 0));
        label_action->setText(QApplication::translate("MainWindow", "Action :", 0));
        label_nbRelance->setText(QApplication::translate("MainWindow", "Nombre de relance :", 0));
        label_pathNas->setText(QApplication::translate("MainWindow", "NAS :", 0));
        label_relanceEnCours->setText(QApplication::translate("MainWindow", "En cours :", 0));
        label->setText(QApplication::translate("MainWindow", "Moniteur webService", 0));
        menuFichier->setTitle(QApplication::translate("MainWindow", "Fichier", 0));
        menuEdition->setTitle(QApplication::translate("MainWindow", "Edition", 0));
        menuAide->setTitle(QApplication::translate("MainWindow", "Aide", 0));
        menuRelance->setTitle(QApplication::translate("MainWindow", "Relance", 0));
        menuMail->setTitle(QApplication::translate("MainWindow", "Mail", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
