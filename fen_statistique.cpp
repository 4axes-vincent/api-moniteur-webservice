#include "fen_statistique.h"
#include "ui_fen_statistique.h"

Fen_statistique::Fen_statistique(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Fen_statistique)
{
    ui->setupUi(this);
    this->setWindowTitle ("Statistiques");

    clikID = 0;
    listerMutuelle ();
}

Fen_statistique::~Fen_statistique()
{
    delete ui;
}








//----------------------------------------------------------------------------------------------------------------------
/*
  Affiche les donn�es sur un graphe � point
  */
void Fen_statistique::afficherGraphPoint(QVector<double> vecteurY){

    ui->widget_stats->clearItems ();

    QVector<double> x( vecteurY.size () );

    double moyenne = 0;

    for (int i = 0; i < vecteurY.size () ; i++){

        moyenne += vecteurY.at (i);
        x[i] = i+1;
    }

    QCPItemText *textLabel = new QCPItemText( ui->widget_stats );
//    ui->widget_stats->addItem(textLabel);
    textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
    textLabel->position->setType(QCPItemPosition::ptAxisRectRatio);
    textLabel->position->setCoords(0.5, 0); // place position at center/top of axis rect
    textLabel->setText( "Frontal -> " % contexte % " - Moyenne : " % QString::number (moyenne/100) % " s");

    int lastValue = vecteurY.at ( vecteurY.size () - 1 );
    int maxValue = lastValue;

    for(int j = 0 ; j < vecteurY.size () ; j++){

        if( maxValue < vecteurY.at (j) )
            maxValue = vecteurY.at (j);
    }


    // create graph and assign data to it:
    ui->widget_stats ->addGraph();
    ui->widget_stats ->graph(0)->setData(x, vecteurY);

    //Legend
    ui->widget_stats->xAxis->setLabel("Nombre de r�conciliation");
    ui->widget_stats->yAxis->setLabel("Temps de r�conciliation");

    // set axes ranges, so we see all data:
    ui->widget_stats->xAxis->setRange(0, vecteurY.size ());
    ui->widget_stats->yAxis->setRange(0, maxValue + 5);
    ui->widget_stats->replot();
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Liste les RNM des mutuelles utilis�es dans la base de donn�es
  */
void Fen_statistique::listerMutuelle(){

    Database *bdd = new Database();
    connect (bdd, SIGNAL(on_erreur(QString)), this, SLOT(envoyerMessage(QString)));
    connect (bdd, SIGNAL(on_message(QString)), this, SLOT(envoyerMessage(QString)));

    QStringList listeMutuelle = bdd->getListeRnm ();

    if( !listeMutuelle.isEmpty ())
        ui->comboBox_mutuelle->addItems ( listeMutuelle );

    else
        emit on_message ("Aucune mutuelle en base de donn�es", true);
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur une date du calendrier
  */
void Fen_statistique::on_calendrier_clicked(const QDate &date){

    if( ui->checkBox_journeeStats->isChecked () || ui->checkBox_periodeStats->isChecked () ){

        if( ui->checkBox_journeeStats->isChecked () )
            ui->lineEdit_dateDebut->setText ( date.toString ("dd/MM/yyyy") );

        else if( ui->checkBox_periodeStats->isChecked () ){

            if( clikID == 0 ){

                ui->lineEdit_dateDebut->setText ( date.toString ("dd/MM/yyyy") );
                clikID = 1;
            }
            else{

                ui->lineEdit_dateFin->setText ( date.toString ("dd/MM/yyyy") );
                clikID = 0;
            }
        }
    }
    else
        emit on_message ("Veuillez s�lectionner le crit�re journ�e ou p�riode", true);
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur la checkBox "journ�e"
  */
void Fen_statistique::on_checkBox_journeeStats_clicked(bool checked){

    ui->checkBox_periodeStats->setChecked ( false );
    ui->checkBox_periodeStats->setEnabled ( !checked );

    ui->label->setEnabled ( !checked );
    ui->lineEdit_dateFin->setEnabled ( !checked );
    ui->lineEdit_dateFin->clear ();

    ui->lineEdit_dateDebut->setText ( QDate::currentDate ().toString ("dd/MM/yyyy") );
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur la checkBox "p�riode"
  */
void Fen_statistique::on_checkBox_periodeStats_clicked(bool checked){

    ui->checkBox_journeeStats->setChecked ( false );
    ui->checkBox_journeeStats->setEnabled ( !checked );

    ui->lineEdit_dateDebut->clear ();
    ui->lineEdit_dateFin->clear ();
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "G�n�rer statistique"
  N�cessite : journ�e coch�e + date OU p�riode coch�e + date de d�but et date de fin
  */
void Fen_statistique::on_btn_genererStats_clicked(){

    if( ui->checkBox_journeeStats->isChecked () || ui->checkBox_periodeStats->isChecked () ){

        if( ui->checkBox_journeeStats->isChecked () && ui->lineEdit_dateDebut->text ().isEmpty () )
            emit on_message ("Veuillez s�lectioner une date de recherche", true);

        else if ( ui->checkBox_periodeStats->isChecked () && ( ui->lineEdit_dateDebut->text ().isEmpty () || ui->lineEdit_dateFin->text ().isEmpty () ) )
            emit on_message ("Veuillez s�lectionner une p�riode de recherche", true);

        else{

            Database *bdd = new Database();
            QVector<double> vecteurY;

            if( ui->radioButton_4axes->isChecked () || ui->radioButton_webService->isChecked () ){

                if( ui->checkBox_journeeStats->isChecked () ){

                    if( ui->radioButton_4axes->isChecked () )
                        vecteurY = bdd->getStats ( QDate::fromString ( ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd"),
                                                   "-1" , "tempsTransmission", ui->comboBox_mutuelle->currentText ());

                    else if( ui->radioButton_webService->isChecked () )
                        vecteurY = bdd->getStats ( QDate::fromString ( ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd"),
                                                   "-1", "tempsRetour", ui->comboBox_mutuelle->currentText () );
                }

                else if ( ui->checkBox_periodeStats->isChecked () ){

                    if( ui->radioButton_4axes->isChecked () )
                        vecteurY = bdd->getStats ( QDate::fromString ( ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd"),
                                                    QDate::fromString ( ui->lineEdit_dateFin->text (), "dd/MM/yyyy").toString ("yyyyMMdd"), "tempsTransmission",
                                                   ui->comboBox_mutuelle->currentText ());

                    else if( ui->radioButton_webService->isChecked () )
                        vecteurY = bdd->getStats ( QDate::fromString ( ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd"),
                                                     QDate::fromString ( ui->lineEdit_dateFin->text (), "dd/MM/yyyy").toString ("yyyyMMdd"), "tempsRetour",
                                                   ui->comboBox_mutuelle->currentText ());
                }

                if( !vecteurY.isEmpty () ){

                    delete bdd;
                    afficherGraphPoint ( vecteurY );
                }
                else
                    emit on_message ("Aucun r�sultat", true);
            }
            else
                emit on_message ("Veuillez s�lectionner un crit�re de recherche", true);
        }
    }
    else
        emit on_message ("Veuillez s�lectionner un crit�re de recherche", true);
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite exporter ces r�sultats (format PDF)
  */
void Fen_statistique::on_btn_exporter_clicked(){

    if( ui->widget_stats->savePdf ( PATH_EXPORT % "/Export_stats_temps_" % contexte % "_"
                                    % QDateTime::currentDateTime ().toString ("yyyyMMdd_hh_mm_ss") %".pdf",1000,500) )
        QMessageBox::information (NULL,"Export statistique", "Statistiques export�es et envoy�es pour contr�le");

    else
        emit on_message ("Impossible d'exporter les statistiques", true);
}













//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur s�lectionne le radioButton "webService"
  */
void Fen_statistique::on_radioButton_webService_clicked(){

    contexte = ui->radioButton_webService->text ();
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur s�lectionne le radioButton "4AXES"
  */
void Fen_statistique::on_radioButton_4axes_clicked(){

    contexte = ui->radioButton_4axes->text ();
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'envoyer un message � la classe appelante
  */
void Fen_statistique::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte );
}
