#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>
#include <QThread>
#include <QString>
#include <QStringBuilder>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QPair>
#include <QList>
#include <QSettings>
#include <QMutex>

#include <QDomDocument>
#include <QXmlStreamReader>
#include <QtXmlPatterns/QXmlQuery>

#include "configuration.h"
#include "database.h"


#define PATH_DOSSIER_CONFIGURATION              QString(QCoreApplication::applicationDirPath () % "/Configuration")
#define PATH_DOSSIER_HISTORY                    QString(QCoreApplication::applicationDirPath () % "/History")

#define PATH_FICHIER_CONFIG                     QString(QCoreApplication::applicationDirPath () % "/config.ini")

class Message : public QThread
{
    Q_OBJECT

public:
    Message();

    void parserRetourWs( QString idChronoDPEC, QString messageRetour, QString rnm, QString contexte );

    void parserListeReponse(QString pathDossier);

private slots:
    void envoyerMessage(QString message, bool alerte);

    void setPauseIteration(bool flag);

signals:
    void on_message(QString message, bool alerte);

    void on_traitementFini();

    void on_envoyerVers4axes(QString idChronoDpec, QString fluxRetour, QString etat, QString libelle);

    void on_dpecSuivante();

    void on_FinRenvoi();

    void on_decoderPec(QString fichierPec, QString idChrono);

    void on_deplacerFichier(QString pathFichierSourice, QString pathFichierCible, bool couperFichier);

private:

    bool pauseIteration;
};

#endif // MESSAGE_H
