#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "dpecscrute.h"
#include "configuration.h"
#include "message.h"
#include "database.h"
#include "fen_statistique.h"
#include "form_stats.h"
#include "relance.h"
#include "emailing.h"
#include "QNetworkConfiguration"
#include "fichier.h"





#include <QMainWindow>
#include <QMessageBox>
#include <QString>
#include <QStringBuilder>
#include <QStringList>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QtConcurrent/QtConcurrentRun>
#include <QUrlQuery>
#include <QUrl>
#include <QTimer>
#include <QDateTime>
#include <windows.h>
#include <QRegExp>
#include <QPair>
#include <QTextEdit>
#include <QFileDialog>
#include <QProcess>
#include <QCalendarWidget>
#include <QInputDialog>



#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkProxy>
#include <QtNetwork/QSslConfiguration>
#include <QtNetwork/QSslCertificate>
#include <QtNetwork/QSslKey>


#define NOM_FICHIER_LOG                   QString(QCoreApplication::applicationDirPath () % "/moniteur.log")
#define NOM_FICHIER_LOG_RELANCE           QString(QCoreApplication::applicationDirPath () % "/relance_" % QDate::currentDate ().toString ("yyyyMMdd") % ".log")
#define NOM_FICHIER_LOG_ANNULATION        QString(QCoreApplication::applicationDirPath () % "/annulation_" % QDate::currentDate ().toString ("yyyyMMdd") % ".log")
#define NOM_FICHIER_TEMP_EDITION_PEC      QString(QCoreApplication::applicationDirPath () % "/requeteEditionPec.xml")
#define PATH_FICHIER_CONFIG               QString(QCoreApplication::applicationDirPath () % "/config.ini")

#define PATH_DOSSIER_HISTORY              QString(QCoreApplication::applicationDirPath () % "/History")
#define PATH_DOSSIER_CONFIGURATION        QString(QCoreApplication::applicationDirPath () % "/Configuration")
#define PATH_DOSSIER_FICHIER              QString(QCoreApplication::applicationDirPath () % "/Fichier")
#define PATH_DOSSIER_CERTIFICAT           QString(QCoreApplication::applicationDirPath () % "/Certificat")
#define PATH_TEMP                         QString(QCoreApplication::applicationDirPath () % "/Temp")
#define PATH_ERR_ENVOI_XML                QString(QCoreApplication::applicationDirPath () % "/ERREUR_ENVOI_XML/" % QDate::currentDate ().toString ("yyyyMMdd") )
#define PATH_ERR_RETOUR_XML               QString(QCoreApplication::applicationDirPath () % "/ERREUR_RETOUR_XML")
#define PATH_ERR_NOM_FICHIER_DPEC         QString(QCoreApplication::applicationDirPath () % "/ERREUR_NOM_DPEC")
#define PATH_DOSSIER_ANNULATION           QString(QCoreApplication::applicationDirPath () % "/DOSSIER_ANNULATION/" % QDate::currentDate ().toString ("yyyyMMdd") )
#define PATH_DOSSIER_LOG_ERREUR           QString(QCoreApplication::applicationDirPath () % "/LOG_ERREUR")
#define PATH_RETOUR_PDF                   QString(QCoreApplication::applicationDirPath () % "/RetourPDF")

#define PATH_EXE_CURL                     QString(QCoreApplication::applicationDirPath () % "/CURL/curl.exe")

#define HTTP_DOMAINE_4AXES                QString("domaine")
#define HTTP_DOMAINE_4AXES_VALEUR         QString("4axes")

//Local
//#define ADR_HTTP_RETOUR_XML             QString("http://192.168.1.12/transmissions/httpsdocs/api/integrerxml")
//#define ADR_HTTP_RECONCILIER            QString("http://192.168.1.12/transmissions/httpsdocs/api/reconcillier")

//Pr�-prod
//#define ADR_HTTP_RETOUR_XML               QString("https://pp.4axes.net/api/integrerxml")
//#define ADR_HTTP_RECONCILIER              QString("https://pp.4axes.net/api/reconcillier")

//Prod
#define ADR_HTTP_RETOUR_XML             QString("https://4axes.net/api/integrerxml")
#define ADR_HTTP_RECONCILIER            QString("https://4axes.net/api/reconcillier")


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void ecrireLog(QString pathFichier, QString info, bool horodatage);

    void ecrireIHM(QString info);

    void debugger(QString titre, QString info);

    void creerFichierVisualisation(QString resultat, QString idChronoDPEC);

    void envoyerMessage(QString nomFichierXML, QString contexteAction);

    void lancerRelance();

    void modifierFichier(QString pathFichierXml, QString idChrono);

    void parsertest();

    void testTelechargerRpec(QString rnm);

    void chargerLog();    

    void recupererErreur();

    void lancerArchivage();

    void recupererSansReponses();

    void visualiserDossier(QList<QPair<QString,QString> >listeDossier);

    void genererPDF( QStringList listParametre, QString rnm, QString etatRetour, QString libelleRefus );

    void telechargerRpec(QString rnm);

    void parserReponse(QString reponse, QString contexte);

    bool deplacerFichier(QString nomFichier, QString repDestination, QString sens);

    bool enregistrerFichierPdf(QByteArray fluxPdf, QString pathDestination, QString idChrono);

    QString creerFichierEditionPec(QString rnmMutuelle);

    QString getRnm(QString idChronoDPEC);

    QString lireContenu(QString pathFichier);

    QStringList creeerParametre(QString retourXml, QString rnmMutuelle);

    void integrerRetour(QString idChronoDpec, QStringList listeDonnees, QString etat, QString libelleRefus);

    void popperRpec();

signals:
    void on_stop_iteration(bool flag);

    void on_pause_relance(bool flag);

    void on_pause_traitementMail(bool flag);

private slots:
    void envoyerMail(QString message);

    void envoyerPieceJointe(QString pathFichierLog, QString message, QString sujet, QString to);

    void renvoyerMessage(QString pathFichierXml, QString contexteAction);

    void reconcilierMessage(QString pathFichier, QString idChronoDpec);

    void retourRetourRest(QNetworkReply *reply);

    void finPop();

    void lancerAPI();

    void deplacerFichier(QString pathFichierSource, QString pathFichierDestination, bool couperFichier);

    void fichierEnCours(QString nomFichier);

    void retourEnvoiPecWs(QNetworkReply *reply);

    void relancerScrute();

    void retourEnvoi4Axes(QNetworkReply * reply);

    void preparerRetourPec(QString idChronoDpec, QString rpecXML, QString etat, QString libelle);

    void on_actionRapport_d_erreur_triggered();

    void on_actionStatistiques_triggered();

    void popUp(QString message);

    void on_actionEnvoyer_RPEC_triggered();

    void on_actionConsulter_RPEC_triggered();

    void renvoiFini();

    void on_actionStatistique_refus_triggered();

    void visualierPEC(QString idChronoDPEC, QString dateDPEC);

    void finRelance();

    void on_actionTest_envoi_mail_triggered();

    void on_actionRelancer_depuis_une_date_triggered();

    void selectionDateRelance(QDate date);

    void incrementerCompteurRelance(int relanceEnCours);

    void on_actionStatistiques_retour_triggered();

    void afficherMessage(QString message, bool alerte);

    void retourFichierPec(QNetworkReply *reply);

    void decoderFlux(QString donnee, QString idChronoPec);

    void on_actionT_l_charger_RPEC_PDF_triggered();

    void on_actionPopper_triggered();

    void on_actionTraiter_PDF_triggered();

private:
    Ui::MainWindow *ui;

    Database OBJ_bdd;

    Configuration config;

    QTime *chrono;

    QDateTime dateTime;

    bool reconciliationEnCours;

    bool siRelance;

    bool renvoiRpec;

    bool receptionMail;

    QString idChrono;

    QString rnmMutuelleEnCours;

    QString typeAction;

    QString suffixe;

    QString dateRelance;

    QString pathFichierPdf;

    QFileInfo fichierInfoXML;

    QTextEdit *textEdit;

    int nbErreurEnvoi;

    int nbRelanceEnCours;
};

#endif // MAINWINDOW_H
