#ifndef FORM_STATSRETOUR_H
#define FORM_STATSRETOUR_H

#include <QWidget>

namespace Ui {
class Form_statsRetour;
}

class Form_statsRetour : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_statsRetour(QWidget *parent = 0);
    ~Form_statsRetour();
    
private:
    Ui::Form_statsRetour *ui;
};

#endif // FORM_STATSRETOUR_H
