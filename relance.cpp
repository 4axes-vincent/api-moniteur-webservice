#include "relance.h"

Relance::Relance()
{
}



//----------------------------------------------------------------------------------------------------------------------
void Relance::relancer(QList<QPair<QString,QString> > listeRelance, QString pathConfig){

    Configuration *config = new Configuration();

    Database *bdd = new Database();
    connect (bdd, SIGNAL(on_erreur(QString)), this, SLOT(envoyerMessage(QString)));

    QString idChrono;
    QString dateDpec;
    QString valeurRelance;

    //On parcourt la liste des idChrono concern�s par les relances
    for(int i = 0 ; i < listeRelance.length () ; i++){

        emit on_incrementerVisualisation(i+1);

        idChrono = listeRelance[i].first;
        dateDpec = listeRelance[i].second;

        valeurRelance = config->getValue ( pathConfig % "/" % getRnm ( idChrono ) % "_DPEC.ini", "Action","relance");

        //Si la mutuelle est configur�e pour �tre relanc�e
        if( valeurRelance == "1" ){

            QThread::currentThread ()->msleep (250);

            //Mise � jour de la derni�re date de relance
            if( !bdd->majDonnee ( idChrono, "dateRelance", QDate::currentDate ().toString ( "yyyyMMdd" ) ) )
                emit on_message ("Echec lors de la mise � jour de la date relance de l'ICHRONO " % idChrono, true);

            else{

                //Mise � jour du nombre de relance
                if( !bdd->incrementerRelance ( idChrono ) )
                    emit on_message ("Echec lors de l'incr�mentation des relances", true);

                else{

                    pause = true;

                    emit lancerVisualisation( idChrono, dateDpec );

                    do{
                        QThread::currentThread ()->msleep (3000);
                    }while( pause );
                }
            }
        }
        else if ( valeurRelance == "-1")
            emit on_message (config->getValue ( pathConfig, "Libelle_erreur", "0x0002") % getRnm ( idChrono ) % ".ini", false);

        else
            emit on_message ( "La mutuelle " % getRnm ( idChrono) % " n'est pas configuree pour �tre relancee", false );

        QThread::currentThread ()->msleep (1000);
    }

    delete config;
    delete bdd;

    emit traitementFiniRelance ();

    QThread::currentThread ()->quit ();
}














//----------------------------------------------------------------------------------------------------------------------
void Relance::pauseRelance(bool flag){

    pause = flag;
}












//----------------------------------------------------------------------------------------------------------------------
void Relance::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte );
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner le RNM de l'organisme depuis une IDCHRONO donn�
  */
QString Relance::getRnm(QString idChronoDPEC){

    return( idChronoDPEC.split ("_").at (1) );
}
