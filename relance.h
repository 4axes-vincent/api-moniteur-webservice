#ifndef RELANCE_H
#define RELANCE_H

#include <QObject>
#include <QThread>
#include <QString>
#include <QStringBuilder>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QSettings>
#include <QMutex>
#include <QDateTime>
#include <QPair>

#include "database.h"
#include "configuration.h"


class Relance : public QThread
{
    Q_OBJECT

public:
    Relance();

    void relancer(QList<QPair<QString, QString> > listeRelance, QString pathConfig);
    QString getRnm(QString idChronoDPEC);

private slots:
    void pauseRelance(bool flag);

    void envoyerMessage(QString message, bool alerte);

signals:
    void lancerVisualisation(QString chrono, QString date);

    void on_message(QString message, bool alerte);

    void traitementFiniRelance();

    void on_incrementerVisualisation(int i);

private:

    bool pause;
};


#endif // RELANCE_H
